#!/bin/sh

############ TEST DATA ############
# Use this variables to change mandatory unique fields in the tests
# Institiution (max lenght 4)
export INST="WBK"
# Branch (max lenght 8)
export BRANCH="TSTBRN01"
# Customer (max lenght 8)
export CUSTOMER="MICHALJG"
# Account (max lengh 12)
export ACCOUNT=""

# Those have no need to be changed between each test
# First Name (max lenght 20)
export FIRSTNAME="MICHAL"
# Last Name (max lenght 20)
export LASTNAME="GROCHMAL"
# Address (3 lines, max lenght 35 for each)
export ADDRLINE1="ADDRESS LINE 1"
export ADDRLINE2="ADDRESS LINE 2"
export ADDRLINE3="ADDRESS LINE 3"
# Home City (max lenght 20)
export HOMECITY="LONDON"
# Home Telephone (max lenght 20)
export HOMETEL="123123123123"
# Home postcode (max lenght 20)
export HOMEPOST="NWC TOI"
# Work Address (3 lines, max lenght 35 for each)
export WORKADDRLINE1="WORK ADRESS LINE 1"
export WORKADDRLINE2="WORK ADRESS LINE 2"
export WORKADDRLINE3="WORK ADRESS LINE 3"
# Work City (max lenght 20)
export WORKCITY="LONDON"
# Increment the test, increment by 2
TESTV=`cat test.version`
TESTV=`expr $TESTV + 2`
echo "***************** TEST VERSION IS $TESTV ***************************"
echo $TESTV > test.version
export TEST_NUMBER=$TESTV
# customer number used in tests
export CUSTOMER=`expr 11223344 + $TEST_NUMBER`
# account number for this customer
export ACCOUNT=`expr 100022 + $TEST_NUMBER`
# This card fails due to invalid BIN_RANGE
export NEW_CARDPAN_FAIL=`expr 4104107200000001 + $TEST_NUMBER`
# This card is used in first test as OK
export NEW_CARDPAN=`expr 4104107200000000 + $TEST_NUMBER`
# This pan is used for card revewal!
export NEW_CARDPAN_2=`expr 4104107200000000 + $TEST_NUMBER + 1`
# This is VPAN which is not extracted, with pan from different IID
# Using for replacement test too!
export VPAN_OUT_OF_IID="123456HH00150865"
# Replace lost or stolen... VPAN from different IID side
export VPAN_OUT_OF_IID_2="123456HH00152475"
################# SOME CONFIG ############################################
PRE_DIR=data
DATA_DIR=tmp
OUT_DIR=out
#USER=mgrochma
#HOST=kurosawa
#TARG_DIR=/share/homes/preserve/QA/BZWBK-10-005-A/data
#UD_DIR=ud
################# CLEAN UP OLD STUFF #####################################
rm out/*
rm tmp/*
################# REPLACE THE TEST DATA ##################################
data_files=`ls -C1 $PRE_DIR`
for file in $data_files ; do
	echo "env processing from [$PRE_DIR/$file] to [$DATA_DIR/$file.sdi]";
	./env2mark.pl $PRE_DIR/$file $DATA_DIR/$file.sdi || exit -1
done
######################### PROCESS THE UD #################################
#data_files=`ls -C1 $UD_DIR`
#for file in $data_files ; do
#	echo "env processing from [$UD_DIR/$file] to [$UD_DIR/$file.sdi]";
#	./env2mark.pl $UD_DIR/$file $OUT_DIR/$file || exit -1
#done
################# DO THE BUILD ###########################################
data_files=`ls -C1 $DATA_DIR`
for file in $data_files ; do
	echo "processing from [$DATA_DIR/$file] to [$OUT_DIR/$file.sdi]";
	./create_sdi.pl $DATA_DIR/$file > $OUT_DIR/$file.sdi || exit -1
done
################# DO THE PUBLISH #########################################
#for file in  $OUT_DIR/*; do
#	echo "Copying [$file $USER@$HOST:$TARG_DIR]"
#	scp $OUT_DIR/* $USER@$HOST:$TARG_DIR
#done	

echo "done"
