#!/bin/sh
# translation of primers in shell
>DNA.fas
echo ">dna1" >> DNA.fas
echo CCATGATACTTTTTAGAGAGCAGATTCTGATGTTTAGCGTAATACTAATGTTCAGAGAG >> DNA.fas
echo CAGATCCTGGCCGTAATGTTCAGGGAGCAAAGGGAGCAAAGGGAGCAAGCGTTCATGCA >> DNA.fas
echo GTTAAATGGTATCACAAATGGTATAGGGAACAATGATTGA >> DNA.fas

echo -ne "dna sense:\t"
tail -n 3 DNA.fas | tr -d '\r\n'
echo

echo -ne "dna antisense:\t"
tail -n 3 DNA.fas | tr -d '\r\n' | tr TACG ATGC
echo

echo

echo -ne "forward primer:\t"
tail -n 3 DNA.fas | tr -d '\r\n' | head -c 20
echo

echo -ne "reverse primer:\t"
tail -n 3 DNA.fas | tr -d '\r\n' | tr TACG ATGC | tail -c 20 | tac -rs '.'
echo

rm DNA.fas

