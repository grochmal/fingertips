#!/bin/sh

# Wrapper for pinentry to ask for a password directly from a tty and then
# continue to use this password from the shell script.  The password stays in
# memory and is not displayed anywhere before the last echo.

ask_password() {
  TTY=$(tty)
  pass=$(cat <<EOF | pinentry 2>/dev/null | tail -n +6
OPTION lc-ctype=$LANG
OPTION ttyname=$TTY
SETTITLE MySQL user
SETPROMPT password:
GETPIN
EOF
  )
  echo $pass | sed -e 's/^D //' -e 's/ OK$//'
  return 0
}
ps=$(ask_password)
echo $ps

