#!/bin/sh

# Runs travis tests from the command line.  Must be placed at the root of the
# project to find the correct python path, yet can be run from anywhere as it
# enforces the directory structure by itself.

usage () {
    echo "Usage: run-travis-tests.sh <tests-directory> [pattern]"
    echo
    echo "    pattern - glob pattern to use to search for test files in the"
    echo "              tests directory.  If not provided 'test*.py' is used."
}

# Enforce directory structure, `realpath` is FreeBSD compatible.
DIR=$(dirname $(realpath "$0"))
cd $DIR

run () {
    touch "$1/__init__.py"
    P2=$(which python2 2>/dev/null)
    P3=$(which python3 2>/dev/null)
    for p in $P2 $P3
    do
        echo $p -m unittest discover -t . -s \"$1\" -p \"$2\"
        $p -m unittest discover -t . -s "$1" -p "$2"
    done
    rm "$1/__init__.py"
}

if test "x$2" = "x"
then
    PATTERN='test*.py'
else
    PATTERN="$2"
fi

if test "x$1" = "x"
then
    usage
elif ! test -d "$1" -a -r "$1" -a -x "$1"
then
    echo "$1: no such directory"
else
    TESTS=`realpath "$1"`
    run "$TESTS" "$PATTERN"
fi

