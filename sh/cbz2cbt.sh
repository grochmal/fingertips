#!/bin/sh

# configuration
RETOUCH=1
TMP=/tmp

# OS dependent stuff
#REALPATH=realpath
REALPATH="readlink -e"  # redhat 6 hack

check_names () {
    ls *.jpeg 2>/dev/null
    ret=$?
    if test 0 -eq $ret
    then
        return 1
    fi
    ls * |
        perl -ne 'chomp; print "$_\n" if /[^a-z0-9-~.]/' |
        grep .
    ret=$?
    if test 0 -eq $ret
    then
        return 1
    fi
    return 0
}

retouch () {
    for i in `ls -1 *.jpg 2>/dev/null`
    do
        echo "mogrify -strip -resize '1280x1920>^' -quality 80 \"$i\""
        mogrify  -strip -resize '1280x1280>^' -quality 80 "$i"
    done
    for i in `ls -1 *.jpeg 2>/dev/null`
    do
        j=`basename "$i" .jpeg`.jpg
        echo "convert \"$i\" -strip -resize '1280x1280>^' -quality 80 \"$j\""
        convert "$i" -strip -resize '1280x1280>^' -quality 80 "$j"
        echo rm \"$i\"
        rm "$i"
    done
    for i in `ls -1 *.png 2>/dev/null`
    do
        j=`basename "$i" .png`.jpg
        echo "convert \"$i\" -strip -resize '1280x1280>^' -quality 80 \"$j\""
        convert "$i" -strip -resize '1280x1280>^' -quality 80 "$j"
        echo rm \"$i\"
        rm "$i"
    done
}

repack () {
    # inform the user about what we will do
    RELCBZ="$1"
    echo processing: $RELCBZ

    # build all variables
    CBZ=`basename "$RELCBZ"`
    ORGDIR=`$REALPATH $(dirname "$RELCBZ")`
    TMPDIR="${TMP%/}/"`id -u`-$$-`date +%s`-cbz2cbt
    ORGBASEDIR=`pwd -P`
    name=`basename "$CBZ" .cbz`
    if test "$name" = "$CBZ"
    then
        name=`basename "$CBZ" .zip`
    fi
    if test "$name" = "$CBZ"
    then
        echo ERROR: $CBZ :The extension must be .cbz or .zip, ABORTING
        return
    fi
    CBT="$name.cbt"
    FULLCBZ="$ORGDIR/$CBZ"
    FULLCBT="$ORGDIR/$CBT"

    # check is we actually have something to do
    if ! test -f "$ORGDIR/$CBZ"
    then
       echo "ERROR: $ORGDIR/$CBZ : no such file, ABORTING"
       return
    fi

    # create the temporary directory
    if test -e "$TMPDIR"
    then
        echo -n "ERROR: $CBZ : $TMPDIR : "
        echo "directory exists, will not overwrite, ABORTING"
        return
    fi
    echo mkdir \"$TMPDIR\"
    mkdir "$TMPDIR"

    # unzip the CBZ into the temporary directory
    cp "$ORGDIR/$CBZ" "$TMPDIR"
    cd "$TMPDIR"
    unzip "$CBZ"
    ret=$?
    if test 0 -ne $ret
    then
        echo "ERROR: $CBZ : not a zip file, ABORTING"
        cd "$ORGBASEDIR"
        rm -rf "$TMPDIR"
        return
    fi
    rm "$TMPDIR/$CBZ"

    # convert/mogrify the images
    if ! expr 0 = "$RETOUCH" >/dev/null
    then
        echo Retouching images...
        retouch
    fi

    # check if all is sanitised, prompt the user if not
    if ! check_names
    then
       echo ERROR: $CBZ : NOT SANITISED NAMES found, ABORTING.  Check $TMPDIR
       cd "$ORGBASEDIR"
       return
    fi
    if ls *.gif 2>/dev/null
    then
       echo ERROR: $CBZ : I cannot deal with GIF images, sorry.  Check $TMPDIR
       cd "$ORGBASEDIR"
       return
    fi

    # all is good, tar it all together and place it next to the CBZ
    FILES=`ls -1 *.jpg *.png 2>/dev/null | sort | xargs`
    echo tar cvf \"$CBT\" $FILES
    tar cvf "$CBT" $FILES
    mv "$CBT" "$ORGDIR"
    cd "$ORGBASEDIR"

    # check if everything went alright
    CBZFILES=`unzip -l "$FULLCBZ" | tail -n 1 | tr -s ' ' | cut -d ' ' -f 3`
    CBTFILES=`tar tf "$FULLCBT" | wc -l`
    echo $CBZ : $CBZFILES files
    echo $CBT : $CBTFILES files
    if test $CBZFILES -eq $CBTFILES
    then
        rm -rf "$TMPDIR"
    else
        echo ERROR: $CBT : Something went WRONG, check $TMPDIR
    fi
}

if ! which unzip >/dev/null
then
    echo unzip missing, ABORTING
elif ! which tar >/dev/null
then
    echo tar missing, ABORTING
elif ! which mogrify >/dev/null
then
    echo "mogrify (image magick) missing, ABORTING"
else
    for a in "$@"
    do
        repack "$a"
        sleep 6
    done
fi

