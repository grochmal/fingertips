/*-----------------------------------------------------------------------------
 *
 * Function     : SERVICE?
 *
 * Purpose      : Service entry point for TODO
 *
 * Parameters   : Usual tpcall
 *
 * Returns      : tpreturns
 *
 * Comments     : 
 *
 *---------------------------------------------------------------------------*/
public void SERVICE/* TODO */(TPSVCINFO *p_svc)
{
	int ret = SUCCEED;
	char *svc = SERVICESVC;  /* TODO */
	FBFR *p_fb = MAKEBIG(p_svc->data);

	DBG_SETLEV(M_dbglev);
	DBG_SETBUF(M_dbgbuf);
	DBG_SETNAMEBYPAN(p_fb);  /* TODO */
	DBG_SETDUMPMODE(p_fb, DUMP_ATSTART);
	DBG_STAR(("%s service", svc));
	DBG_FBDIFFSTART(p_fb);

/* TODO Does this service need to use SQL? */
	if(0 == ntp_getlev())
	{
		DBG_PRINTF((dbg_syserr, "Tuxedo transaction in progress - "
			    "service can't use SQL."));
		ret = FAIL;
	}
	else if(SUCCEED != (ret = sql_begin()))
	{
		DBG_PRINTF((dbg_syserr, "Could not sql_begin."));
		ev_beginfail(0L);
	}
	else
	{
/*
 ************************** Service body goes here. ***************************
 */
		if(SUCCEED == ret)
			sql_commit();

		if(SUCCEED != ret)
			sql_abort();
	}

	DBG_PRINTF((dbg_proginfo, "ret = %s",
		    SUCCEED == ret ? "SUCCEED" : "FAIL"));

	DBG_FBDIFFEND(p_fb,DIFF_CLOSE);
	DBG_STAR(("ntp_return from: %s", svc));
	DBG_CLOSEFILE();

	ntp_return(SUCCEED == ret ? TPSUCCESS : TPFAIL,
		   0L, (char*)p_fb, 0L, 0L);
}


