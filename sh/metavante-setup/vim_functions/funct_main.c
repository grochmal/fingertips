#define TAGNAME		"CFGSECTION"

private int M_dbglev = dbg_progdetail;  /* Debug trace output level. */
private int M_dbgbuf = 0;  /* Debug buffering level. */
private char M_dbgfile[CTX_FILENAME_MAX] = {0};  /* Debug filename. */

/*-----------------------------------------------------------------------------
 *
 * Function     : main
 *
 * Purpose      : Test harness.
 *
 * Parameters   : argc, argv - Usual suspects.
 *
 * Returns      : SUCCEED - On expected behavior.
 *                FAIL - If something goes wrong.
 *
 * Comments     :
 *
 *---------------------------------------------------------------------------*/
public int main(int argc, char **argv)
{
	int ret = SUCCEED;
	static long ctxdate;
	static clp_parm clp[] =
	{
		{'d', parm_long, 0, FALSE, (void *)&ctxdate, 0},
		{0}
	};
	static cfp_parm cfp[] =
        {
		{"DEBUG", parm_int, 0, FALSE, (void *)&M_dbglev, 0},
		{"DBGBUF", parm_int, 0, FALSE, (void *)&M_dbgbuf, 0},
		{"DBGFILE", parm_string, sizeof(M_dbgfile), FALSE,
			(void *)&M_dbgfile, 0},
		{0}
	};


	if(SUCCEED != (ret = clp_parse(argc, argv, clp)))
	{
		fprintf(stderr, "Failed to parse command line.\n");
	}
	else if(SUCCEED != (ret = cfp_lite(cfp, TAGNAME, NULL /* subsect */)))
	{
		fprintf(stderr, "Failed to parse configuration file.\n");
	}

	if(SUCCEED == ret)
	{
		DBG_SETFILE(M_dbgfile);
		DBG_SETLEV(M_dbglev);
		DBG_SETBUF(M_dbgbuf);

		if(SUCCEED != (ret = sql_open(NULL)))
		{
			DBG_PRINTF((dbg_syserr, "Failed to open database."));
		}
		else if(SUCCEED != (ret = sql_begin()))
		{
			DBG_PRINTF((dbg_syserr, "Failed to begin SQL."));
		}
	}

	/* Functionality goes here. */

	if(SUCCEED == ret)
		ret = sql_commit();

	if(SUCCEED != ret)
		ret = sql_abort();

	return ret;
}


