#!/bin/sh

# Cleans a mixture of ascii, microsoft and iso_8859-1
# encoding into pure ascii encoding.

# 0x80: windows-1251 euro sign            => E=
# 0x82: windows-1251 single quote         => '
# 0x83: windows-1251 function sign        => f
# 0x84: windows-1251 double quote         => "
# 0x85: windows-1251 ellipsis             => ...
# 0x86: windows-1251 dagger               => +
# 0x87: windows-1251 double dagger        => ++
# 0x88: windows-1251 carret               => ^
# 0x89: windows-1251 permille             => /1000
# 0x8a: windows-1251 capital strong s     => S
# 0x8b: windows-1251 less than            => <
# 0x8c: windows-1251 capital letter ce    => CE
# 0x8e: windows-1251 capital strong z     => Z
# 0x91: windows-1251 backquote            => '
# 0x92: windows-1251 acute accent         => '
# 0x93: windows-1251 left "smart" quote   => "
# 0x94: windows-1251 right "smart" quote  => "
# 0x95: windows-1251 middle dot           => .
# 0x96: windows-1251 dash                 => -
# 0x97: windows-1251 dash                 => -
# 0x98: windows-1251 tilde                => ~
# 0x99: windows-1251 trademark sign       => (TM)
# 0x9a: windows-1251 small strong s       => s
# 0x9b: windows-1251 more than            => >
# 0x9c: windows-1251 small letter ce      => ce
# 0x9e: windows-1251 small strong z       => z
# 0x9f: windows-1251 capital umlaut y     => Y

# 0xa0: iso_8859-1 non breakable space     => 0x20
# 0xa1: iso_8859-1 inverted exclamation    => !
# 0xa2: iso_8859-1 cent sign               => c
# 0xa3: iso_8859-1 pound sign              => P
# 0xa4: iso_8859-1 currency sign           => C
# 0xa5: iso_8859-1 yen sign                => Y
# 0xa6: iso_8859-1 broken bar              => |
# 0xa7: iso_8859-1 section sign            => S
# 0xa8: iso_8859-1 diaeresis               => "
# 0xa9: iso_8859-1 copyright sign          => (C)
# 0xaa: iso_8859-1 fenimine ordinal        => ^a
# 0xab: iso_8859-1 left double angle       => <<
# 0xac: iso_8859-1 not sign                => -
# 0xad: iso_8859-1 soft hyphen             => -
# 0xae: iso_8859-1 registered sign         => (R)
# 0xaf: iso_8859-1 macron                  => ^-
# 0xb0: iso_8859-1 degree sign             => ^o
# 0xb1: iso_8859-1 plus minus              => +-
# 0xb2: iso_8859-1 superscript two         => ^2
# 0xb3: iso_8859-1 superscript three       => ^3
# 0xb4: iso_8859-1 acute accent            => '
# 0xb5: iso_8859-1 micro sign              => m
# 0xb6: iso_8859-1 pilcrow                 => |P
# 0xb7: iso_8859-1 middle dot              => *
# 0xb8: iso_8859-1 cedilla                 => ,
# 0xb9: iso_8859-1 superscript one         => ^1
# 0xba: iso_8859-1 masculine ordinal       => ^o
# 0xbb: iso_8859-1 right double angle      => >>
# 0xbc: iso_8859-1 one quarter             => 1/4
# 0xbd: iso_8859-1 one half                => 1/2
# 0xbe: iso_8859-1 three quarters          => 3/4
# 0xbf: iso_8859-1 inverted question mark  => ?

# 0xc6: iso_8859-1 capital letter ae    => AE
# 0xd0: iso_8859-1 capital letter eth   => =D
# 0xd7: iso_8859-1 multiplication sign  => *
# 0xd8: iso_8859-1 capital stroke       => 0
# 0xdf: iso_8859-1 sharp s              => B
# 0xe6: iso_8859-1 letter ae            => ae
# 0xf0: iso_8859-1 letter eth           => =d
# 0xf7: iso_8859-1 division sign        => /
# 0xf8: iso_8859-1 stroke               => 0

sed -e 's/\x80/E=/'     \
    -e "s/\x82/'/g"     \
    -e 's/\x83/f/g'     \
    -e 's/\x84/"/g'     \
    -e 's/\x85/.../g'   \
    -e 's/\x86/+/g'     \
    -e 's/\x87/++/g'    \
    -e 's/\x88/^/g'     \
    -e 's=\x89=\1000=g' \
    -e 's/\x8a/S/g'     \
    -e 's/\x8b/</g'     \
    -e 's/\x8c/CE/g'    \
    -e 's/\x8e/Z/g'     \
    -e "s/\x91/'/g"     \
    -e "s/\x92/'/g"     \
    -e 's/\x93/"/g'     \
    -e 's/\x94/"/g'     \
    -e 's/\x95/./g'     \
    -e 's/\x96/-/g'     \
    -e 's/\x97/-/g'     \
    -e 's/\x98/~/g'     \
    -e 's/\x99/(TM)/g'  \
    -e 's/\x9a/s/g'     \
    -e 's/\x9b/>/g'     \
    -e 's/\x9c/ce/g'    \
    -e 's/\x9e/z/g'     \
    -e 's/\x9f/Y/g'     \
    $@ |
sed -e 's/\xa0/ /g'     \
    -e 's/\xa1/!/g'     \
    -e 's/\xa2/c/g'     \
    -e 's/\xa3/P/g'     \
    -e 's/\xa4/C/g'     \
    -e 's/\xa5/Y/g'     \
    -e 's/\xa6/|/g'     \
    -e 's/\xa7/S/g'     \
    -e 's/\xa8/"/g'     \
    -e 's/\xa9/(C)/g'   \
    -e 's/\xaa/^a/g'    \
    -e 's/\xab/<</g'    \
    -e 's/\xac/-/g'     \
    -e 's/\xad/-/g'     \
    -e 's/\xae/(R)/g'   \
    -e 's/\xaf/^-/g'    \
    -e 's/\xb0/^o/g'    \
    -e 's/\xb1/+-/g'    \
    -e 's/\xb2/^2/g'    \
    -e 's/\xb3/^3/g'    \
    -e "s/\xb4/'/g"     \
    -e 's/\xb5/m/g'     \
    -e 's/\xb6/|P/g'    \
    -e 's/\xb7/*/g'     \
    -e 's/\xb8/,/g'     \
    -e 's/\xb9/^1/g'    \
    -e 's/\xba/^o/g'    \
    -e 's/\xbb/>>/g'    \
    -e 's=\xbc=1/4=g'   \
    -e 's=\xbd=1/2=g'   \
    -e 's=\xbe=3/4=g'   \
    -e 's/\xbf/?/g'     \
    -e 's/\xc6/AE/g'    \
    -e 's/\xd0/=D/g'    \
    -e 's/\xd7/*/g'     \
    -e 's/\xd8/0/g'     \
    -e 's/\xdf/B/g'     \
    -e 's/\xe6/ae/g'    \
    -e 's/\xf0/=d/g'    \
    -e 's=\xf7=/=g'     \
    -e 's/\xf8/0/g'

