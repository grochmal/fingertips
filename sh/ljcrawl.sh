#!/bin/sh

BASEURL="http://mangable.com/files/images/love_junkies"

if test -z "$1" -o -z "$2" \
|| expr "$2" : '[^0-9]' > /dev/null
then
  echo "Usage: $0 <chapter no.> <no. of pages>"
  exit 1
else
  echo "chapter: $1, no. $2"
fi

page="1"
ret="0"
while test "$page" -le "$2" -a "0" = "$ret"
do
  echo wget "$BASEURL/$1/$page.jpg"
  wget "$BASEURL/$1/$page.jpg"
  ret="$?"
  echo "returned: $ret"
  if test "0" = "$ret"
  then
    name=`printf "love-junkies-c%03i-p%03i\n" "$1" "$page"`
    echo mv "$page.jpg" "$name.jpg"
    mv "$page.jpg" "$name.jpg"
  fi
  page=`expr "$page" + 1`
  sleep 6
done

