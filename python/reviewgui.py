#!/bin/env python

######
# Review GUI
######

import Tkinter

class ReviewGUI:
    def __init__(self):
        self.rootWindow = Tkinter.Tk()
        self.rootWindow.title('p4 Review')

        # Menu
        self.windowMenu = Tkinter.Menu(self.rootWindow)

        self.fileMenu = Tkinter.Menu(self.windowMenu)
        self.fileMenu.add_command(label='New')
        self.fileMenu.add_command(label='Append to...')
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label='Exit')
        self.windowMenu.add_cascade(label='File', menu=self.fileMenu)

        self.clientsMenu = Tkinter.Menu(self.windowMenu)
        self.machineClientsMenu = Tkinter.Menu(self.clientsMenu)
        self.machineClientsMenu.add_command(label='Reload')
        self.machineClientsMenu.add_separator()
        self.machineClientsMenu.add_command(label='Use machine clients')
        self.clientsMenu.add_cascade(label='Machine clients', \
            menu=self.machineClientsMenu)
        self.otherClientsMenu = Tkinter.Menu(self.clientsMenu)
        self.otherClientsMenu.add_command(label='New')
        self.otherClientsMenu.add_command(label='Clean client list')
        self.otherClientsMenu.add_separator()
        self.otherClientsMenu.add_command(label='Use other clients')
        self.clientsMenu.add_cascade(label='Other clients', \
            menu=self.otherClientsMenu)
        self.windowMenu.add_cascade(label='Clients', menu=self.clientsMenu)

        self.jobsMenu = Tkinter.Menu(self.windowMenu)
        self.jobsMenu.add_command(label='New')
        self.jobsMenu.add_command(label='Recently used jobs')
        self.jobsMenu.add_command(label='Clean job list')
        self.windowMenu.add_cascade(label='Jobs', menu=self.jobsMenu)

        self.helpMenu = Tkinter.Menu(self.windowMenu)
        self.helpMenu.add_command(label='Tutorial')
        self.helpMenu.add_command(label='About')
        self.windowMenu.add_cascade(label='Help', menu=self.helpMenu)

        self.rootWindow.configure(menu=self.windowMenu)

        # Toolbar
        self.toolBar = Tkinter.Frame(self.rootWindow)

        self.leftFrame = Tkinter.Frame(self.toolBar)

        self.clientType = Tkinter.Label(self.leftFrame, text='Machine clients:')
        self.clientType.pack(anchor='w')

        self.listFrame = Tkinter.Frame(self.leftFrame)
        self.clientScrollBar = Tkinter.Scrollbar(self.listFrame, \
            orient='vertical')
        self.clientList = Tkinter.Listbox(self.listFrame, height=3, \
            yscrollcommand=self.clientScrollBar.set, exportselection=0)
        self.clientList.insert('end', 'client 1')
        self.clientList.insert('end', 'client 2')
        self.clientList.insert('end', 'client 3')
        self.clientList.insert('end', 'client 4')
        self.clientList.insert('end', 'client 5')
        self.clientList.insert('end', 'client 6')
        self.clientScrollBar.configure(command=self.clientList.yview)
        self.clientScrollBar.pack(side='right', fill='y')
        self.clientList.pack(side='left', fill='both', expand=1)
        self.listFrame.pack()

        self.leftFrame.pack(side='left')

        self.rightFrame = Tkinter.Frame(self.toolBar)

        self.jobsLabel = Tkinter.Label(self.rightFrame, text='Jobs:')
        self.jobsLabel.pack(anchor='w')

        self.jobsFrame = Tkinter.Frame(self.rightFrame)
        self.jobsScrollBar = Tkinter.Scrollbar(self.jobsFrame, \
            orient='vertical')
        self.jobsList = Tkinter.Listbox(self.jobsFrame, height=3, \
            yscrollcommand=self.jobsScrollBar.set, exportselection=0)
        self.jobsList.insert('end', 'job 1')
        self.jobsList.insert('end', 'job 2')
        self.jobsList.insert('end', 'job 3')
        self.jobsList.insert('end', 'job 4')
        self.jobsList.insert('end', 'job 5')
        self.jobsList.insert('end', 'job 6')
        self.jobsList.insert('end', 'job 7')
        self.jobsList.insert('end', 'job 8')
        self.jobsList.insert('end', 'job 9')
        self.jobsList.insert('end', 'job 10')
        self.jobsList.insert('end', 'job 11')
        self.jobsList.insert('end', 'job 12')
        self.jobsScrollBar.configure(command=self.jobsList.yview)
        self.jobsScrollBar.pack(side='right', fill='y')
        self.jobsList.pack(side='left', fill='both', expand=1)
        self.jobsFrame.pack()

        self.rightFrame.pack(side='left')

        self.mkPatch = Tkinter.Button(self.toolBar, text='Make Patch')
        self.mkPatch.pack(side='left', anchor='s')

        self.toolBar.pack(side='top', fill='x')

        # Statusbar
        self.statusBar = Tkinter.Frame(self.rootWindow)
        self.statusText = Tkinter.Label(self.statusBar, \
            text='Creating a new patch file')
        self.statusText.pack(side='left')
        self.statusBar.pack(side='bottom', fill='x')

        # Text Pane
        self.textFrame = Tkinter.Frame(self.rootWindow)
        self.textScrollBar = Tkinter.Scrollbar(self.textFrame, \
            orient='vertical')
        self.textPane = Tkinter.Text(self.textFrame, width=12, height=12, \
            yscrollcommand=self.textScrollBar.set)
        self.textScrollBar.configure(command=self.textPane.yview)
        self.textScrollBar.pack(side='right', fill='y')
        self.textPane.pack(side='left', fill='both', expand=1)
        self.textFrame.pack(fill='both', expand=1)

        self.rootWindow.mainloop()

window = ReviewGUI()

