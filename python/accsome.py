#!/usr/bin/env python

# commandline window search for the maximum 3 values in a list.

import sys

def sliding_max(wdw, val):
    c,v = val
    for i in range(len(wdw)):
        if wdw[i][0] < c:
            wdw = wdw[:i] + [(c,v)] + wdw[i:-1]
            print wdw
            break;
    return wdw

def sliding_max_test(window_size, values):
    wdw = [(0,None) for i in range(window_size)]
    for val in values: wdw = sliding_max(wdw, val)
    return wdw

print sliding_max_test(3, [(int(i),i) for i in sys.argv[1:]])

