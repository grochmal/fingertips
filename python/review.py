#!/bin//env python
######
# Review.py
#
# Windows GUI for the code review script
######

# imports
import os
import re
#import Tkinter

# Linux
class LinuxStructure:
    def __init__(self):
        # Default parameters
        self.defaultConfDir = '/etc/p4review/conf'
        self.defaultConfFile = self.defaultConfDir + '/config'
        # Substitute variables
        homeDir = os.environ['HOME']
        # Simple regexes for reading the config file
        re_IsComment = re.compile('^\s*[#\n].*')
        re_FindKey = re.compile('^([^=]*)=')
        re_ChangeHome = re.compile('\$HOME')
        re_PickConfDir = re.compile('^ConfDir=(.*)')
        re_PickConfFile = re.compile('^ConfFile=(.*)')
        re_PickBinDir = re.compile('^BinDir=(.*)')
        re_PickBinFile = re.compile('^BinFile=(.*)')
        re_PickTempDir = re.compile('^TempDir=(.*)')
        re_PickP4Bin = re.compile('^P4Bin=(.*)')
        re_PickUserHasConfig = re.compile('^UserHasConfig=(.*)')
        re_PickUserConfDir = re.compile('^UserConfDir=(.*)')
        # Dictionaries for looping through the cnfig file
        self.parmDict = {}
        self.reDict = {
            'ConfDir' : re_PickConfDir,
            'ConfFile' : re_PickConfFile,
            'BinDir' : re_PickBinDir,
            'BinFile' : re_PickBinFile,
            'TempDir' : re_PickTempDir,
            'P4Bin' : re_PickP4Bin,
            'UserHasConfig' : re_PickUserHasConfig,
            'UserConfDir' : re_PickUserConfDir
            }

        # This should be done with os.stat()
        # but must wait for days when I get more time to work on it
        if(0 != os.system('ls ' + self.defaultConfDir + ' > /dev/null 2>&1')):
            print 'The basic configuration directory', self.defaultConfDir
            print 'Could not be found'
            print 'Creating directory...'
            os.makedirs(self.defaultConfDir, 0755)
            print 'Creating default configuration file...'
            f_ConfigFile = open(self.deafultConfFile, 'w')
            f_ConfigFile.write('ConfDir=/etc/p4review/conf\n')
            f_ConfigFile.write('ConfFile=config\n')
            f_ConfigFile.write('BinDir=/usr/bin\n')
            f_ConfigFile.write('BinFile=p4review.py\n')
            f_ConfigFile.write('TempDir=$HOME/tmp\n')
            f_ConfigFile.write('UserHasConfig=0\n')
            f_ConfigFile.write('UserConfDir=$HOME/.p4review\n')
            f_ConfigFile.close()
            print 'Preparing to read the new configuration file'

        if os.stat(self.defaultConfFile):
            self.f_ConfigFile = open(self.defaultConfFile)
            self.line = self.f_ConfigFile.readline()
            while '' != self.line:
                self.genericMatch = re_IsComment.match(self.line)
                if None == self.genericMatch:
                    self.keyMatch = \
                        re_FindKey.match(self.line)
                    if None != self.keyMatch:
                        self.genericMatch = \
                            self.reDict[self.keyMatch.group(1)].match(self.line)
                        self.parmDict[self.keyMatch.group(1)] = \
                            self.genericMatch.group(1)
                    else:
                        print 'Error in configuration file!'
                        print 'Please check /etc/p4review/conf/config'
                        print 'At line:', self.line, '\n'
                self.line = self.f_ConfigFile.readline()

            # Substitute the $HOME variable in the configuration parameters
            for parm in self.parmDict:
                self.parmDict[parm] = \
                re_ChangeHome.sub(homeDir, self.parmDict[parm])

        print 'Configuration file read, configuration parameters:'
        print self.parmDict

# Windows
class WindowsStructure:
    def __init__(self):
        # Default parameters
        self.defaultConfDir = 'c:\Program Files\p4review\conf'
        self.defaultConfFile = self.defaultConfDir + '\config'
        # Simple regexes for reading the config file
        re_IsComment = re.compile('^\s*[#\n].*')
        re_FindKey = re.compile('^([^=]*)=')
        re_ChangeHome = re.compile('\$HOME')
        re_PickConfDir = re.compile('^ConfDir=(.*)')
        re_PickConfFile = re.compile('^ConfFile=(.*)')
        re_PickBinDir = re.compile('^BinDir=(.*)')
        re_PickBinFile = re.compile('^BinFile=(.*)')
        re_PickTempDir = re.compile('^TempDir=(.*)')
        re_PickP4Bin = re.compile('^P4Bin=(.*)')
        re_PickUserHasConfig = re.compile('^UserHasConfig=(.*)')
        re_PickUserConfDir = re.compile('^UserConfDir=(.*)')
        # Dictionaries for looping through the cnfig file
        self.parmDict = {}
        self.reDict = {
            'ConfDir' : re_PickConfDir,
            'ConfFile' : re_PickConfFile,
            'BinDir' : re_PickBinDir,
            'BinFile' : re_PickBinFile,
            'TempDir' : re_PickTempDir,
            'P4Bin' : re_PickP4Bin
            }

        # This should be done with os.stat()
        # but must wait for days when I get more time to work on it
        if(0 != os.system('dir \"' + self.defaultConfDir + '\" > NUL')):
            print 'The basic configuration directory', self.defaultConfDir
            print 'Could not be found'
            print 'Creating directory...'
            os.makedirs(self.defaultConfDir, 0755)
            print 'Creating default configuration file...'
            f_ConfigFile = open(self.deafultConfFile, 'w')
            f_ConfigFile.write('ConfDir=/etc/p4review/conf\n')
            f_ConfigFile.write('ConfFile=config\n')
            f_ConfigFile.write('BinDir=/usr/bin\n')
            f_ConfigFile.write('BinFile=p4review.py\n')
            f_ConfigFile.write('TempDir=$HOME/tmp\n')
            f_ConfigFile.write('UserHasConfig=0\n')
            f_ConfigFile.write('UserConfDir=$HOME/.p4review\n')
            f_ConfigFile.close()
            print 'Preparing to read the new configuration file'

        if os.stat(self.defaultConfFile):
            self.f_ConfigFile = open(self.defaultConfFile)
            self.line = self.f_ConfigFile.readline()
            while '' != self.line:
                self.genericMatch = re_IsComment.match(self.line)
                if None == self.genericMatch:
                    self.keyMatch = \
                        re_FindKey.match(self.line)
                    if None != self.keyMatch:
                        self.genericMatch = \
                            self.reDict[self.keyMatch.group(1)].match(self.line)
                        self.parmDict[self.keyMatch.group(1)] = \
                            self.genericMatch.group(1)
                    else:
                        print 'Error in configuration file!'
                        print 'Please check /etc/p4review/conf/config'
                        print 'At line:', self.line, '\n'
                self.line = self.f_ConfigFile.readline()

        print 'Configuration file read, configuration parameters:'
        print self.parmDict

class Checks:
    def __init__(self, tempDir):
        self.tempDir = tempDir
        # Regex to find if the job exists
        self.re_FindJob = None
        self.jobName = None

    def checkJob(self, jobName):
        self.jobName = jobName
        self.re_FindJob = re.compile('^' + self.jobName)
        os.system('p4 jobs > \"' + self.tempDir + '\p4_jobs.tmp\"')
        f_AllJobs = open(self.tempDir + 'p4_jobs.tmp')
        line = f_AllJobs.readline()
        while '' != line:
            genericMatch = self.re_FindJob.match(line)
            if None != genericMatch:
                break

            line = f_AllJobs.readline()

        f_AllJobs.close()
        if None == genericMatch:
            print 'This p4 job (' + self.jobName + ') does not exist'
            exit(1)

        os.remove(self.tempDir + 'p4_jobs.tmp')

class MachineClients:
    def __init__(self, tempDir):
        self.tempDir = tempDir
        # Finding hostname
        self.re_HostName = re.compile('\s*Host Name[\s\.]*:\s*([^\s]*)')
        self.hostName = None
        # Finding p4 clients
        self.re_GetClient = re.compile('^Client\s*([^\s]*)')
        self.re_FindClient = None
        self.clientList = None
        self.selectedClient = None
        # Finding client root
        self.re_ClientRoot = re.compile('^Root:\s*(.*)')
        self.clientRoot = None

    def findHostName(self):
        print 'Trying to find this machine hostname'
        os.system('ipconfig /all > \"' + self.tempDir + 'hostname.tmp\"')
        f_HostName = open(self.tempDir + 'hostname.tmp')
        line = f_HostName.readline()
        while '' != line:
            genericMatch = self.re_HostName.match(line)
            if None != genericMatch:
                break

            line = f_HostName.readline()

        if None == genericMatch:
            print 'Could not find hostname, unable to contiue, aborting'
            exit(1)
        
        f_HostName.close()
        os.remove(self.tempDir + 'hostname.tmp')
        self.hostName = genericMatch.group(1)
        print 'Host Name:', self.hostName
        # Dynamically modify regex to find clients
        self.re_FindClient = re.compile('^.*' + self.hostName, re.IGNORECASE)

    def findClients(self):
        print 'Trying to find p4 client useb by this machine'
        if None == self.hostName:
            print 'Unable to find hostname, aborting'
            exit(1)

        self.clientList = []
        os.system('p4 clients > \"' + self.tempDir + 'p4_clients.tmp\"')
        f_P4Clients = open(self.tempDir + 'p4_clients.tmp')
        line = f_P4Clients.readline()
        while '' != line:
            genericMatch = self.re_FindClient.match(line)
            if None != genericMatch:
                clientMatch = self.re_GetClient.match(line)

                if None != clientMatch:
                    self.clientList.append(clientMatch.group(1))

            line = f_P4Clients.readline()

        f_P4Clients.close()
        os.remove(self.tempDir + 'p4_clients.tmp')
        if not len(self.clientList):
            print 'Could not find p4 clients for this machine using hostname'
            print 'The List of clients is empty, you will need to specify'
            print 'the p4 client yourself'
        else:
            print 'Client(s) found'
            print 'Client list:', self.clientList

    def selectClient(self):
        print 'Selecting client from client list'
        if None == self.clientList:
            print 'The client list is empty, aborting'

        print '******'
        print 'Select client'
        # Force counter to int object
        counter = 0
        for counter in range(0, len(self.clientList)):
            print '[' + str(counter) + ']', self.clientList[counter]

        print '******'
        counter = int(raw_input(' $ '))
        if len(self.clientList) <= counter:
            print 'Inexistent client, aborting'
            exit(1)

        self.selectedClient = self.clientList[counter]
        print 'Selected client:', self.selectedClient

    def goToClientRoot(self):
        print 'Changing directory to the root of the selected p4 client'
        os.system('p4 client -o ' + self.selectedClient + ' > \"' \
            + self.tempDir + 'client_spec.tmp\"')
        f_ClientSpec = open(self.tempDir + 'client_spec.tmp')
        line = f_ClientSpec.readline()
        while '' != line:
            genericMatch = self.re_ClientRoot.match(line)
            if None != genericMatch:
                self.clientRoot = genericMatch.group(1)
                break

            line = f_ClientSpec.readline()

        f_ClientSpec.close()
        os.remove(self.tempDir + 'client_spec.tmp')
        print 'Client root:', self.clientRoot
        os.chdir(self.clientRoot)

    def testClientRoot(self):
        os.system('dir > zzz')
        print 'Made a file named \'zzz\' in the client root for testing purpose'

class ChangedFiles:
    def __init__(self, usedClient, jobName, tempDir):
        self.usedClient = usedClient
        self.jobName = jobName
        self.tempDir = tempDir
        # List for changelists
        self.changeList = None
        # Regex to get the change number
        self.re_FindChange = re.compile('.*change\s*([0-9]*)\s*')
        # Lists for found files
        self.filesEdited = None
        self.filesAdded = None
        self.filesDeleted = None
        # Regex to find files
        self.re_EditType = re.compile('^([^#]*)#[0-9]*\s-\s([^\s]*)\s')
        # Pickup data about edited file
        self.re_EditFile = re.compile('^====\s*([^\s]*)\s-\s([^\s]*)')
        # Pickup data about file from p4 where
        self.re_WhereFile = re.compile('^([^\s]*)\s[^\s]*\s([\s]*)')
        # Output PatchFile
        self.f_PatchFile = None

    def pickupChangelists(self):
        print 'Tring to find all the changelists used by job', self.jobName
        self.changeList = []
        os.system('p4 fixes -j ' + self.jobName + ' > \"' + self.tempDir \
            + 'p4_fixes.tmp\"')
        f_Fixes = open(self.tempDir + 'p4_fixes.tmp')
        line = f_Fixes.readline()
        while '' != line:
            genericMatch = self.re_FindChange.match(line)
            if None != genericMatch:
                self.changeList.append(genericMatch.group(1))

            line = f_Fixes.readline()

        f_Fixes.close()
        os.remove(self.tempDir + 'p4_fixes.tmp')
        if not len(self.changeList):
            print 'Unable to find changelists fixing to the job', self.jobName
            print 'Check if you are using this job for your project'
            print 'Unable to continue without changelists, aborting'
            self.changeList = None
            exit(1)

        print 'Found Changelists'
        print 'Changelists', self.changeList

    def pickupFiles(self):
        print 'Going to pickup all opened files on the client', self.usedClient
        print 'by the job', self.jobName
        if None == self.changeList:
            print 'Changelist where not found for this job, or werre never'
            print 'searched, cannot continue without changelists, aborting'
            exit(1)

        self.filesEdited = []
        self.filesAdded = []
        self.filesDeleted = []
        for change in self.changeList:
            os.system('p4 opened -c ' + change + ' > \"' + self.tempDir \
                + 'opened_files.tmp\"' + ' 2>&1')
            f_OpenedFiles = open(self.tempDir + 'opened_files.tmp')
            line = f_OpenedFiles.readline()
            while '' != line:
                genericMatch = self.re_EditType.match(line)
                if None != genericMatch:
                    if 'edit' == genericMatch.group(2):
                        self.filesEdited.append(genericMatch.group(1))
                    elif 'add' == genericMatch.group(2):
                        self.filesAdded.append(genericMatch.group(1))
                    elif 'delete' == genericMatch.group(2):
                        self.filesDeleted.append(genericMatch.group(1))
                    elif 'integrate' == genericMatch.group(2):
                        self.filesEdited.append(genericMatch.group(1))
                    elif 'branch' == genericMatch.group(2):
                        self.filesAdded.append(genericMatch.group(1))

                line = f_OpenedFiles.readline()

            f_OpenedFiles.close()
            os.remove(self.tempDir + 'opened_files.tmp')

        if len(self.filesEdited):
            print 'Found edited / integrated files:'
            print self.filesEdited
        else:
            print 'No edited / integrated files on this job'
            self.filesEdited = None

        if len(self.filesAdded):
            print 'Found added / branched files:'
            print self.filesAdded
        else:
            print 'No added / branched files on this job'
            self.filesAdded = None

        if len(self.filesDeleted):
            print 'Found deleted files:'
            print self.filesDeleted
        else:
            print 'No deleted files on this job'
            self.filesDeleted = None

    def processFiles(self):
        print 'Processing found files'
        doneSth = False
        self.f_PatchFile = open(self.tempDir + self.jobName + '.patch', 'w')
        if None != self.filesEdited:
            self.processEdit()
            doneSth = True

        if None != self.filesAdded:
            self.processAdd
            doneSth = True

        if None != self.filesDeleted:
            self.processDelete
            doneSth = True

        self.f_PatchFile.close()
        if doneSth:
            print 'Files processed into file ' + self.jobName + '.patch'
        else:
            print 'No files where found / processed!'
            print 'It may mean that you didn\'t linked the job with a change'

    def processEdit(self):
        print 'Processing edited / integrated files'
        for file in self.filesEdited:
            print 'file:', file
            os.system('p4 diff -dU10000 ' + file + ' > \"' + self.tempDir \
                + 'edited_file.tmp')
            f_EditFile = open(self.tempDir + 'edited_file.tmp')
            line = f_EditFile.readline()
            self.f_PatchFile.write(line)
            fileNameMatch = self.re_EditFile.match(line)
            if None == fileNameMatch:
                print 'Application level error while processing an edited file'
                exit(1)

            # Create header lines
            self.f_PatchFile.write('--- ' + fileNameMatch.group(1) + '\n')
            self.f_PatchFile.write('+++ ' + fileNameMatch.group(2) + '\n')
            line = f_EditFile.readline()
            while '' != line:
                self.f_PatchFile.write(line)
                line = f_EditFile.readline()

            f_EditFile.close()
            os.remove(self.tempDir + 'edited_file.tmp')

    def processAdd(self):
        print 'Processing added / branched files'
        for file in self.filesAdded:
            print 'file:', file
            os.system('p4 where ' + file + ' > \"' + self.tempDir \
                + 'where.tmp\"')
            f_Where = open(self.tempDir + 'where.tmp')
            line = f_Where.read()
            fileNameMatch = re_WhereFile.match(line)
            f_Where.close()
            os.remove(self.tempDir + 'where.tmp')
            if None == fileNameMatch:
                print 'Application level error while processing an added file'
                exit(1)

            # Create header lines
            self.f_PatchFile.write('==== ' + fileNameMatch.group(1) + ' - ' \
                + fileNameMatch.group(2) + ' ====\n')
            self.f_PatchFile.write('--- ' + fileNameMatch.group(1) + '\n')
            self.f_PatchFile.write('+++ ' + fileNameMatch.group(2) + '\n')
            f_AddedFile = open(fileNameMatch.group(2))
            lineCounter = 0
            line = f_AddedFile.readline()
            while '' != line:
                lineCounter += 1
                line = f_AddedFile.readline()

            f_AddedFile.close()
            self.f_PatchFile.write('@@ -1,' + str(lineCounter) + ' +1,' \
                + str(lineCounter) + ' @@')
            f_AddedFile = open(fileNameMatch.group(2))
            line = f_AddedFile.readline()
            while '' != line:
                self.f_PatchFile.write('+' + line)
                line = f_AddedFile.readline()

            f_AddedFile.close()

    def processDelete(self):
        print 'Processing deleted files'
        for file in slef.filesDeleted:
            print 'file:', file
            os.system('p4 where ' + file + ' > \"' + self.tempDir \
                + 'where.tmp\"')
            f_Where = open(self.tempDir + 'where.tmp')
            line = f_Where.read()
            fileNameMatch = re_WhereFile.match(line)
            f_Where.close()
            os.remove(self.tempDir + 'where.tmp')
            if None == fileNameMatch:
                print 'Application level error while processing a deleted file'
                exit(1)

            # Create header lines
            self.f_PatchFile.write('==== ' + fileNameMatch.group(1) + ' - ' \
                + fileNameMatch.group(2) + ' ====\n')
            self.f_PatchFile.write('--- ' + fileNameMatch.group(1) + '\n')
            self.f_PatchFile.write('+++ ' + fileNameMatch.group(2) + '\n')
            os.system('p4 print -q ' + fileNameMatch.group(1) + ' > \"' \
                + self.tempDir + 'deleted_file.tmp\"')
            f_DeletedFile = open(self.tempDir + 'deleted_file.tmp')
            lineCounter = 0
            line = f_DeletedFile.readline()
            while '' != line:
                lineCounter += 1
                line = f_DeletedFile.readline()

            f_DeletedFile.close()
            self.f_PatchFile.write('@@ -1,' + str(lineCounter) + ' +1,' \
                + str(lineCounter) + ' @@')
            f_DeletedFile = open(self.tempDir + 'deleted_file.tmp')
            line = f_DeletedFile.readline()
            while '' != line:
                self.f_PatchFile.write('-' + line)
                line = f_DeletedFile.readline()

            f_DeletedFile.close()
            os.remove(self.tempDir + 'where.tmp')

class P4Review:
    def __init__(self):
        self.dirStruct = WindowsStructure()
        self.check = Checks(self.dirStruct.parmDict['TempDir'] + '\\')
        self.machineClients = MachineClients(\
            self.dirStruct.parmDict['TempDir'] + '\\')

    def makePatch(self):
        self.machineClients.findHostName()
        self.machineClients.findClients()
        self.machineClients.selectClient()
        self.machineClients.goToClientRoot()
        #machineClients.testClientRoot()
        print 'Please type the name of the job used for the changes'
        jobName = raw_input(' $ ')
        self.check.checkJob(jobName)
        self.changedFiles = ChangedFiles(self.machineClients.selectedClient, \
            jobName, self.dirStruct.parmDict['TempDir'] + '\\')
        self.changedFiles.pickupChangelists()
        self.changedFiles.pickupFiles()
        self.changedFiles.processFiles()

####
# main
###

p4review = P4Review()
p4review.makePatch()

