#!/bin/env python

import os
import sys
import subprocess

# Set environment in any case so other runtimes and dbs have
# no chance of being affected
print 'Setting environment...'
os.putenv('DBNAME', 'ist_project_run')
print 'Environment set successfully'

# Checking if we are not in some different cortex system
print 'Checking if we are currently in ist system...'
check = subprocess.Popen('check', stdout=subprocess.PIPE)

# The result of the check should look like
# /cortex/runtime/project/ist
# /cortex/runtime/project/ist/bin
# /cortex/runtime/project/ist/out
# /cortex/runtime/project/ist/conf

line = check.stdout.readline()
if '/cortex/runtime/project/ist' != line:
    print 'Check 1 failed'
    print 'Line returned:', line
    print 'Currently you are not in the ist cortex system please'
    print 'run "$ ps" or "$ shell" and select the number to the right of'
    print '/cortex/runtime/project/ist/conf/setist to enter the correct system'
    sys.exit(1)

line = check.stdout.readline()
if '/cortex/runtime/project/ist/bin' != line:
    print 'Check 2 failed'
    print 'Line returned:', line
    print 'Currently you are not in the ist cortex system please'
    print 'run "$ ps" or "$ shell" and select the number to the right of'
    print '/cortex/runtime/project/ist/conf/setist to enter the correct system'
    sys.exit(1)

line = check.stdout.readline()
if '/cortex/runtime/project/ist/out' != line:
    print 'Check 3 failed'
    print 'Line returned:', line
    print 'Currently you are not in the ist cortex system please'
    print 'run "$ ps" or "$ shell" and select the number to the right of'
    print '/cortex/runtime/project/ist/conf/setist to enter the correct system'
    sys.exit(1)

line = check.stdout.readline()
if '/cortex/runtime/project/ist/conf' != line:
    print 'Check 4 failed'
    print 'Line returned:', line
    print 'Currently you are not in the ist cortex system please'
    print 'run "$ ps" or "$ shell" and select the number to the right of'
    print '/cortex/runtime/project/ist/conf/setist to enter the correct system'
    sys.exit(1)

check.stdout.close()
print 'Yes we are' # in the ist system

cmd = None
while 'quit' != cmd and 'q' != cmd and 'exit' != cmd:
    print '*' * 23 + 'Cortex - Ist Connecion Restarter' + '*' * 23
    print 'What do you need?'
    print '[1] Restart cortex tcpip driver on port 33512'
    print '[2] Restart cortex tcpip driver on port 33521'
    print '[3] Restart cortex tcpip driver on port 33524'
    print '*' * 80
    print 'I already restarted the driver but nothing is happening'
    print '[4] Check who is connected on which port to cortex (a smart netstat)'
    print '[5] Restart / reboot entire cortex'
    print 'The reboot will destroy the connection to all three ports, please'
    print 'inform other people testing you are rebooting'
    print 'The reboot may take a while (up to 5 minutes)'
    print '*' * 80
    print 'I rebooted, but now cortex is not responding to anyone, may it'
    print 'be down?'
    print '[6] Is cortex up?'
    print '*' * 80
    print 'To exit type "quit", "q" or "exit"'

    cmd = raw_input('command:')

    if '1' == cmd:
        print 'Restarting port...'
        so.system('ca sp -tISO93DRV -sIST; ca bp -tISO93DRV -sIST')
        print 'Port 33512 restarted'
    if '2' == cmd:
        print 'Restarting port...'
        so.system('ca sp -tISO93DRV -sIST2; ca bp -tISO93DRV -sIST2')
        print 'Port 33521 restarted'
    if '3' == cmd:
        print 'Restarting port...'
        so.system('ca sp -tISO93DRV -sIST3; ca bp -tISO93DRV -sIST3')
        print 'Port 33524 restarted'
    if '4' == cmd:
        netstat = subprocess.Popen(['netstat', '-an'], stdout = subprocess.PIPE)
        values = subprocess.Popen(['grep', '335'], stdin = netstat.stdout,
            stdout = subprocess.PIPE)

        line = values.stdout.readline()
        while '' != line:
            print line
            line = values.stdout.readline()

        values.stdout.close()
        print 'Interpreting the values:'
        print '10.164.101.18 is Cuant'
        print '10.81.101.18 is torimb'

    if '5' == cmd:
        print 'Rebooting...'
        so.system('down; ca b')
        print 'Done!'

    if '6' == cmd:
        print 'Checking...'
        so.system('ca pcp')

out1 = subprocess.Popen('env', stdout = subprocess.PIPE)
out2 = subprocess.Popen(['grep', 'DBNAME'], stdin = out1.stdout,
    stdout = subprocess.PIPE)

line = out2.stdout.readline()
while '' != line:
    print line,
    line = out2.stdout.readline()

out2.stdout.close()

