#!/usr/bin/env python

# tester for the argument passing in python, i always forget how it
# works exactly.

def args(*args, **kwargs):
    for count, thing in enumerate(args) : print count, thing, 'in args'
    for  name, value in kwargs.items()  : print name, value, 'in kwargs'

args(1, 3, 'pinkie pie', fluttershy='rainbow dash')
print '---'
d = {'pony':'twilight' , 'star trek':'enterprise'}
print repr(d)
print '---'
args(*d)
print '---'
args(**d)

