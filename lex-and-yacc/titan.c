/* hash table implementation as in the section 6.6 from K&R ANSI C book */

struct nlist {
	struct nlist *next;  /* next entry in chain */
	char *name;          /* defined name */
	char *defn;          /* replacement text */
};
typedef struct nlist hash_item;

#define HASHSIZE 128

hash_item *
hash_create (int hashsize)
{
	hash_item *nh = calloc (hashsize, sizeof (hash_item));
	return nh;
}

void
hash_delete (hash *h)
{
	hash_item *p;
	p = h;
	while (*p) {
		if (*(p->name)) free (p->name);
		if (*(p->defn)) free (p->defn);
		p++;
	}
	free (h);
	return;
}

/* hash: form hash value for string s */
static
unsigned int
hash_hash (char *s)
{
	unsigned int hashval;
	for (hashval = 0; *s != '\0'; s++)
		hashval = (hashval + *s) % HASHSIZE;
	return hashval;
}

/*static struct nlist *hashtab[HASHSIZE];*/ /* pointer table */

/* lookup: look for s in hashtab */
hash_item *
lookup (char *s)
{
	hash_item *np;
	for (np = hashtab[hash(s)]; np != NULL; np = np->next)
		if (strcmp(s, np->name) == 0)
			return np;  /* found */
	return NULL;                /* not found */
}

/* install: put (name, defn) in hashtab */
hash *
hash_put (hash *tab, char *name, char *defn)
{
    hash *np;
    unsigned hashval;
    if ((np = lookup(name)) == NULL) { /* not found */
        np = (struct nlist *) malloc(sizeof(*np));
        if (np == NULL || (np->name = strdup(name)) == NULL)
          return NULL;
        hashval = hash(name);
        np->next = hashtab[hashval];
        hashtab[hashval] = np;
    } else /* already there */
        free((void *) np->defn); /*free previous defn */
    if ((np->defn = strdup(defn)) == NULL)
       return NULL;
    return np;
}

char *strdup(char *s) /* make a duplicate of s */
{
    char *p;
    p = (char *) malloc(strlen(s)+1); /* +1 for ’\0’ */
    if (p != NULL)
       strcpy(p, s);
    return p;
}

