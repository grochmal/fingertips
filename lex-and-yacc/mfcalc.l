/* flex definitions */

%{
#include <mffcalc.tab.h>
%}

FLOAT  [+-]?([0-9]+\.[0-9]*|\.[0-9]+)([eE][+-]?)?

%%

[0-9]+         yyval = atoi (yytext); return INTEGER
[0-9]\.[0-9]*  |
\.[0-9]+       |

