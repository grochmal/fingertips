/* lexer for xmlstream */
%{
/*#include "xmlstream.tab.h"*/
#define TAG         300
#define ENTITY_REF  301
#define ATTR        302
#define ATTRVAL     303
#define PCDATA      304
%}

WSN         [ \t\r\n]
NSTARTCHAR  [^ \t\r\n<&-.0-9]
NCHAR       [^ \t\r\n<&]
NAME        [^ \t\r\n<&-.0-9][^ \t\r\n<&]*

%x attrs
%x pcdata

%%

<{NAME}   { BEGIN (attrs);
            return TAG; }
&{NAME};  { return ENTITY_REF; }
<![^>]+>   /* ignore declarations */
<?[^?]+?>  /* ignore processing instructions */

<attrs>">"       { BEGIN (pcdata);  }
<attrs>"/>"      { BEGIN (INITIAL); }
<attrs>{NAME}    { return ATTR;     }
<attrs>"\".+\""  { return ATTRVAL;  }
<attrs>=         { return '=';      }

<pcdata>[^<]+    { BEGIN (INITIAL); return PCDATA; }
<pcdata>[^<]{0}  { BEGIN (INITIAL); }

<*>{WSN}+        /* ignore whitespace */

%%

int
main (void)
{
  int r = 0;
  while ( (r = yylex ()) > 0 )
    ;  /* keep going */
}

