#!/usr/bin/env perl
use strict;
use warnings;

use File::Basename;
use Cwd qw(abs_path);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf parse_cmd);
use File::Spec::Functions;

my $cmd = shift || 'help';
my $base = dirname abs_path $0;
my $ext_script = catfile $base, "p4c-$cmd.pl";
if (-x $ext_script) {
    my $script = "$ext_script @ARGV";
    print "Executing [$script]\n";
    system $script;
}
else {
    my ($defs, @args) = parse_cmd @ARGV;
    my $gf = $defs->{'global-flags'} || '';
    my $conf = get_conf;
    my $cf = '';
    $cf = "-c $conf->{client}" if $conf->{client};
    unshift @args, $gf, "-u $conf->{user}", $cf, $cmd;
    (my $script = "$conf->{bin} @args") =~ s/ +/ /g;
    print "Executing [$script]\n";
    exec $script or die "Cannot exec [$script]";
}

__END__

=head1 NAME

p4c.pl - Perforce extension for Cortex development

=head1 SYNOPSIS

 p4c.pl command [arguments --define global-flags=" "]

=head1 DESCRIPTION

Executes either a p4 command or the extended cortex command.  If and extension
exists a script called p4c-command.pl will be called with all arguments, if no
extension can be found p4 is called directly.  The define 'global-flags' is
passed to the p4 command before the command, e.g.

 p4c.pl changes -t -m 12 --define global-flags="-s"

will call:

 p4 -s changes -t -m 12

whilst

 p4c.pl info -s

will call:

 p4c-info.pl -s

For documentation about a extended command run perldoc on the p4c-command.pl
script, or run p4c.pl help command.  Documentation about p4 commands can be
found by calling p4 help command directly.

=cut

