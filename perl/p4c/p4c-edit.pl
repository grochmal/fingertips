#!/usr/bin/env perl
use strict;
use warnings;

use File::Basename;
use Cwd qw(abs_path);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf parse_cmd);

sub add_file_to_job ($$$);  #TODO

print "Not implemented yet!\n";

sub add_file_edit ($$$$$$$) {
    use strict;
    use warnings;
    my ($ctx_flags, $g_flags, $cmd, $args, $user, $pwd, $client) = @_;
    disallow_in_build $pwd;
    print "\n*** You are checking out one or more files. ***\n";
    my $change = add_file_to_job $user, $ctx_flags, $client;
    print "\nGetting latest version...\n";
    my $p4cmd_sync = "p4cl @$g_flags -c $client sync @$args";
    print "$p4cmd_sync\n";
    system $p4cmd_sync;
    print "\nOpening for edit...\n";
    my $p4cmd_edit = "p4cl @$g_flags -c $client edit -c $change @$args\n";
    print "$p4cmd_edit\n";
    system $p4cmd_edit;
}

