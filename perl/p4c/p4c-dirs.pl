#!/usr/bin/env perl
use strict;
use warnings;

use File::Basename;
use Cwd qw(abs_path);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf parse_cmd);

=head2 list_dirs(@ARGV)

Wrapper around p4 dirs command allowing 2 improvements.

1st We can specify no argument where the current directory
will be used as the directory to list it's contents.

2nd The local path can be used in place of the depot path,
the original p4 dirs command allow only depot paths.

=cut
sub list_dirs (@) {
    my $dirs = shift;
    my $where;
    my @flags;
    foreach (@_) {
        if (/^-/) {
            push @flags, $_;
        }
        else {
            $where = $_;
        }
    }
    if (not defined $where) {
        my @paths = `p4 where ...`;
        @paths = grep {!/^-/} @paths;
        ($where = shift @paths) =~ s#/\.\.\. .*#/*#;
    }
    elsif ($where !~ m{^//}) {
        $where =~ s# .*##;
        $where .= '/' unless $where =~ m{/$|\.\.\.$};
        $where .= '...' unless $where =~ m{\.\.\.$};
        my @paths = `p4 where $where`;
        @paths = grep {!/^-/} @paths;
        ($where = shift @paths) =~ s#/\.\.\. .*#/*#;
    }
    else {
        $where =~ s/\.\.\.$//;
        $where =~ s{/$}{/*};
        $where =~ s{([^\*])$}{$1/*};
    }
    my $cmd = 'p4cl ' . $dirs . ' ' . (join ' ', @flags) . ' ' . $where;
    system $cmd;
}

print "Not implemented yet!\n";

