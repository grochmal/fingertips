#!/usr/bin/env perl
use strict;
use warnings;

use File::Basename;
use Cwd qw(abs_path);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf parse_cmd);

my ($cmd) = ($0 =~ /p4c-([^-]+)\.pl$/);
die "\n***\n"
  . "p4c extcmd is the generic extension procedure not a p4 command.\n"
  . "***\n\n" if not $cmd or 'extcmd' eq $cmd;
my ($defs, @args) = parse_cmd @ARGV;
my $gf = $defs->{'global-flags'} || '';
my $conf = get_conf;
print "\n***\n"
    . "WARN:  You are not in a workspace, the output below may be mislading.\n"
    . "Change directory to a workspace for more information.\n"
    . "***\n\n" unless $conf->{client};
my $cf = '';
$cf = "-c $conf->{client}" if $conf->{client};
unshift @args, $gf, "-u $conf->{user}", $cf, $cmd;
(my $script = "$conf->{bin} @args") =~ s/ +/ /g;
print "Executing [$script]\n";
exec $script or die "Cannot exec [$script]";

__END__

=head1 NAME

p4c-extcmd.pl - General extension to a p4 command

=head1 SYNOPSIS

 p4c-<extcmd>.pl [--define global-flags=" "] [desired p4 command flags]

=head1 DESCRIPTION

Works as the intended p4 command but uses the workspace defined in the current
directory, given that there is one.  It also ensures that the proper POSIX
username is used with the command.  If a suitable workspace cannot be found p4
info is executed anyway, but a warning is printed.

For more information about the command itself consult the p4 documentation by
issuing:

 p4 help command

=cut

