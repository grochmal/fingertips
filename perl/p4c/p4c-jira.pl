#!/usr/bin/env perl
use strict;
use warnings;

use File::Basename;
use Cwd qw(abs_path);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf parse_cmd);

sub get_jira_issue ($) {
    use strict;
    use warnings;
    my $jobno = shift;
    die "No job numer given\n" unless $jobno;
    open my $p4cmd, "p4cl -s job -o $jobno |";
    while (<$p4cmd>) {
	next unless m!^info:\s+URL:!;
	my ($jira_issue) = m{info:\s+URL:\s+(.*)$};
	print "$jobno - $jira_issue\n";
    }
    close $p4cmd;
}

print "Not implemented yet!\n";

