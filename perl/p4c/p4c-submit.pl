#!/usr/bin/env perl
use strict;
use warnings;

use File::Basename;
use Cwd qw(abs_path);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf parse_cmd);

sub get_choice ($$);  #TODO

sub check_pending (\%\%$) {
    use strict;
    use warnings;
    my ($jobs, $changes, $ctx_flags) = @_;
    my $job = have_job_option $ctx_flags;
    return undef, undef unless $job;
    unless (exists $jobs->{$job}) {
	warn "Job [$job] do not have a pending changelist, select one:\n";
	return undef, undef;
    }
    return $jobs->{$job}, $changes->{$job};
}

sub submit ($$$$$$$) {
    use strict;
    use warnings;
    my ($ctx_flags, $g_flags, $cmd, $args, $user, $pwd, $client) = @_;
    disallow_in_build $pwd;
    print "\n*** You are submitting a job. ***\n";
    my ($jobs, $changes) = get_jobs_in_progress $user;
    my ($job, $change) = check_pending %$jobs, %$changes, $ctx_flags;
    until ($job) {
	$job = get_choice %$jobs, "Enter the job to submit";
	print "Option 'more' does not make sense for submit (use grep).\n";
    }
    system "p4cl -c $client change $changes->{$job}";
    print "\nSubmitting job $job (change $changes->{$job})\n" .
          "Are you sure you want to submit this change [n] ? ";
    if (<STDIN> =~ /^y/i) {
	print "Doing submit...\n";
	my $p4cmd = "p4cl -c $client submit -c $changes->{$job}";
	print "$p4cmd\n";
	system $p4cmd;
    }
    else {
	print "Aborting submit\n";
    }
}

print "Not implemented yet!\n";

