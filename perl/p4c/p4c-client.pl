#!/usr/bin/env perl
use strict;
use warnings;

use File::Basename;
use Cwd qw(abs_path);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf parse_cmd);

sub client_generate ($$$) {
    use strict;
    use warnings;
    my ($user, $pwd, $host) = @_;
    my $clientroot = $pwd;
    $clientroot =~ s{.*/([^/]+)$}{$1};
    my $clientname = $host . "." . $user . "." . $clientroot;
    print "Would you like to create client $clientname? [n] ";
    if (<STDIN> =~ /^y/i) {
	return $clientname;
    }
    print "Enter the name of the client to create(Enter to quit):\n";
    my $client = <STDIN>;
    chomp $client;
    return undef unless $client;
    return $client;
}

sub client_edit ($$$$$$$$) {
    use strict;
    use warnings;
    my ($ctx_flags, $g_flags, $cmd, $args, $user, $pwd, $client, $host) = @_;
    my $p4cmd;
    if (@$args and not $client) {
	$p4cmd = "p4cl @$g_flags client @$args";
   }
   elsif ($client) {
	$p4cmd = "p4cl @$g_flags -c $client client @$args";
   }
   else {
	print "You are not in a valid client workspace\n";
	my $new_client = client_generate $user, $pwd, $host;
	die "No client, giving up" unless $new_client;
	$p4cmd = "p4cl @$g_flags client $new_client";
    }
    print "$p4cmd\n";
    system $p4cmd;
}

print "Not implemented yet!\n";

