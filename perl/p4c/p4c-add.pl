#!/usr/bin/env perl
use strict;
use warnings;

use File::Basename;
use Cwd qw(abs_path);
use Hash::Util qw(lock_keys);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf_strict parse_cmd lock_configs script2cmd);
use P4CtxUtils qw(disallow_in_build);
use P4CtxJobPrint qw(add2job);

my %scnf = ( 'global-flags'  => undef
	   , nostamps        => undef
	   , stamp           => 'p4stamps.pl'
	   , stamprm         => 'p4stampremove.pl'
	   , 'stamp-flags'   => '--files'
	   , 'stamprm-flags' => '--files'
	   , which           => 'which'
	   , nojob           => undef
	   , jobname         => undef
	   , 'force-build'   => undef
	   );
lock_keys %scnf;

sub bail_stamps ($) {
    my $prog = shift;
    unless (0 == system "$scnf{which} $prog") {
	print "Can't find $prog, bail.  Stamps will not be added.\n";
	return 1;
    }
    return 0;
}

# FIXME the stamps shall be taken directly from a package,
# not included as an extra script.
sub stamp (@) {
    my @files = grep { not /^-/ and -w $_ } @_;
    print "Adding stamps...\n";
    my $stamps_add = "$scnf{stamp} $scnf{'stamp-flags'} @files 2>&1";
    my $stamps_rm  = "$scnf{stamprm} $scnf{'stamprm-flags'} @files 2>&1";
    print "$stamps_add\n";
    open my $stamps, "$stamps_add |";
    while (<$stamps>) {
	if (/^error/) {
	    warn "Can't add stamps [$_], ignoring\n"
	       . "You can also use '--define nostamps' to disable stamps\n";
	    print "$stamps_rm\n";
	    system $stamps_rm or warn "Can't remove added stamps, if any\n";
	    last;
	}
    }
    close $stamps;
}

my ($defs, @args) = parse_cmd @ARGV;
lock_configs %scnf, %$defs;
my $gf = $scnf{'global-flags'} || '';
my $conf = get_conf_strict;

#mandatory_client $conf->{client}, (script2cmd $0);
disallow_in_build $conf->{croot}, (script2cmd $0) unless $scnf{'force-build'};
stamp @args unless $scnf{nostamps} or bail_stamps $scnf{stamp}
				   or bail_stamps $scnf{stmprm};
my $change = add2job $conf, $scnf{jobname} unless $scnf{nojob};
unshift @args, "-c $change" if $change;

my $cf = '';
$cf = "-c $conf->{client}" if $conf->{client};
unshift @args, $gf, "-u $conf->{user}", $cf, 'add';
(my $script = "$conf->{bin} @args") =~ s/ +/ /g;
print "Executing [$script]\n";
exec $script or die "Cannot exec [$script]";

