#!/usr/bin/env perl
package P4Ctx;

use strict;
use warnings;

use Carp;
use Sys::Hostname;
use Cwd qw(:DEFAULT abs_path);
use Hash::Util qw(lock_hash lock_keys);

BEGIN {
    require Exporter;
    our $VERSION   = 0.1;
    our @ISA       = qw(Exporter);
    our @EXPORT_OK = qw(get_conf get_conf_strict
			lock_configs parse_cmd script2cmd);
}

my %conf = ( bin    => 'P4CLBIN'
	   , server => 'P4SERVERCONN'
	   , client => undef
	   , user   => undef
	   , croot  => undef
	   , chost  => undef
	   );
lock_keys %conf;

sub script2cmd ($) { my ($cmd) = ((shift) =~ /(p4c-[^-]+)\.pl$/); $cmd }

sub lock_configs (\%\%) {
    my ($configs, $defines) = @_;
    foreach (keys %$defines) {
	unless (exists $configs->{$_}) {
	    warn "Unknown define [$_], ignoring\n";
	    next;
	}
	$configs->{$_} = $defines->{$_};
    }
    lock_hash %$configs;
    $configs;
}

sub get_user () {
    delete $ENV{USER} if $ENV{USER};
    my $user = $ENV{P4USER};
    if ($user) { chomp $user; warn "Usnig user ($user) from P4USER\n" }
    else       { $user = getpwuid $<                                  }
    $user;
}

sub get_root ($) {
    my $client = shift;
    my $cmd = "$conf{bin} -s -c $client client -o";
    open my $p4_clt, "$cmd |" or confess "Cannot execute [$cmd]: $!";
    my $root;
    while (<$p4_clt>) { last if ($root) = m#^info:\s+Root:\s+(\S+)# }
    close $p4_clt;
    $root;
}

sub get_client ($) {
    my $env_client = $ENV{P4CLIENT};
    if ($env_client) {
	warn "Using client ($env_client) from P4CLIENT\n";
	chomp $env_client;
	return $env_client, get_root $env_client;
    }
    my ($save_client, $save_root);
    my ($cmd, $host, $pwd) = ("$conf{bin} -s clients", shift, abs_path cwd);
    open my $p4_clts, "$cmd |" or confess "Cannot execute [$cmd]: $!";
    while (<$p4_clts>) {
	next unless /^info/;
	my ($client, $root) = m#^info:\s+Client\s+(\S+)\s+\S+\s+root\s+(\S+)#;
	next if $client !~ m#\Q$host\E#;
	next if $pwd    !~ m#^\Q$root\E#;
	next if $save_root and length $root < length $save_root;
	$save_client = $client;
	$save_root = $root;
    }
    close $p4_clts;
    return $save_client, $save_root;
}

sub get_conf () {
    warn "P4PASSWD set, this might reveal your password\n" if $ENV{P4PASSWD};
    if ($ENV{P4CONFIG}) {
	warn "P4CONFIG (set to $ENV{P4CONFIG}) is not supported, ignoring\n";
	delete $ENV{P4CONFIG};
    }
    my %defs;
    my $port = $ENV{P4PORT};
    if ($port) {
	warn "P4PORT set, you will talk to p4 at $ENV{P4PORT}\n";
	chomp $port;
	$defs{server} = $port;
    }
    my $msg  = 'Sorry, I could not figure out';
    $defs{user}   = get_user                or confess "$msg your username";
    ($defs{chost} = hostname) =~ s/\..*$//;
    $defs{chost}                            or confess "$msg the hostname";
    ($defs{client}, $defs{croot}) = get_client $defs{chost};
    lock_configs %conf, %defs;
}

my $name_conv = <<EOS
A workspace will only be identified by p4c if it contains the hostname
(but not necessarily the domain name) of the machine where the worksapce
is used.  For example on the machine perforce.metavanteuk.pri the worskpaces:
  perforce.p4c
  myclient.preforce
  user.perforce
  perforce.metavanteuk.pri.myuser
Are all valed workspace names.
EOS
;

sub get_conf_strict () {
    my $confs = get_conf;
    my $cmd = script2cmd $0;
    croak "You cannot execute the $cmd command outside of a workspace,\n"
        . "to create a workspace use the p4c-client or p4c-workspace\n"
        . "commands.  Also, note the p4c name convention for workspaces:\n"
        . $name_conv . "\n" unless $confs->{client};
    $confs;
}

sub parse_cmd (@) {
    my (%defs, @args);
    while (my $arg = shift) {
	if ('--define' eq $arg) {
	    my ($key, $val) = split /=/, shift;
	    $defs{$key} = $val ? $val : 1;
	}
	else {
	    push @args, $arg;
	}
    }
    \%defs, @args;
}

1;

__END__

=head1 NAME

P4Ctx - extension of p4 client for Cortex development, base package.

=head1 DESCRIPTION

The low level extension is implemented, i.e. the idea of using a different p4
client (workspace) depending on which directory the command is executed from.
This package will locate the client defined in the closest parent directory,
you shall then use this client with p4.  Note that when using NFS the client
must be defined on the same machine you are working one

=head1 SYNOPSIS

Both sample scripts assume that the package is in the same directory:

A generic script to call p4 with the client defined by the file system
directory and the user taken from it's unix username:

 use File::Basename;
 use Cwd qw(abs_path);
 BEGIN { push @INC, dirname abs_path $0 };
 use P4Ctx qw(get_conf parse_cmd);

 my $conf = get_conf;
 my $cmd = shift;
 my ($defs, @args) = parse_cmd @ARGV;
 my $gf = '';
 $gf = $defs->{global_flags} if exists $defs->{global_flags};
 exec "$conf->{bin} -c $conf->{client} -u $conf->{user} $gf $cmd @args";

Or a more strict version with predefined --define arguments

 use Hash::Util qw(lock_keys);
 use File::Basename;
 use Cwd qw(abs_path);
 BEGIN { push @INC, dirname abs_path $0 };
 use P4Ctx qw(get_conf parse_cmd lock_configs);

 my %script_confs = ( nostamps => 0
                    , job      => undef
                    );
 lock_keys %script_confs;
 my $conf = get_conf_strict;
 my ($defs, @args) = parse_cmd @ARGV;
 lock_configs %script_confs, %$defs;

 ... # use %script_confs as an immutable array

 my $cmd = "$conf->{bin} -c $conf->{client} -u $conf->{user} cmd @args";
 open my $p4_call, "$cmd |" or die "Cannot execute [$cmd]: $!";
 while (<$p4_call>) {

 ...


=head1 FUNCTIONS

All functions must be exported into the userspace explicitly, these are:

=over 8

=item get_conf

Returns a reference to a hash containing the information necessary to run an
extension script of p4.  Not all information is always populated, the client
and client root (croot) will contain undef if the call is not executed from
withing a valid workspace.  The values returned are:

bin    => Name of the p4 client binary (it must be in the PATH)

server => The p4 server we wish to talk to

client => Workspace (client) used in the current directory

user   => User that is running the p4 transaction

croot  => Root of the p4 client (workspace) on the file system

chost  => Machine name according to the workspace

=item get_conf_strict

Same as get_conf but fails if client and croot values cannot be populated.

=item parse_cmd (@)

Transforms the command containing --defines into a command suited for the p4
client.  The bad design decision of p4 having arguments before and after the
command is this way removed.  The define "global-flags" shall be used as a
standard to define the arguments that shall go between the p4 binary and the
command.  In the example below p4c is the extension script and p4 the p4
client.
 p4c cmd [--define global-flags="-c client -s" --define ...] [args ...]

is transformed into:
 p4 [global-flags] cmd [args ...]

explicit example:
 p4c where --define global-flags=-s //path/to/file
 p4 -s where //path/to/file

=item lock_configs (\%\%)

Copies all key value pairs from the second hash into the first, given that the
first hash posses the key.  Otherwise the pair is not copied.

It locks and returns the first hash, i.e. no further changes to this hash will
be allowed by perl.

=back

=cut

