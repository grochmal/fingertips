#!/usr/bin/env perl
#use strict;
#use warnings;

use File::Basename;
use Cwd qw(abs_path);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf parse_cmd);

print "Not implemented yet!\n";

# map any depot location to
# a file in the client view
sub map_to_clientview_src_not_base {
	local($from_loc, @cview) = @_;

	$from_tmp = $from_loc;
	while( $from_tmp =~ /\/src\// )
	{
		$from_tmp =~ s/(.*)\/src\/.*/\1/;
	}

	$from_src = $from_loc;
	$from_src =~ s/$from_tmp\///;

	$mapto = `p4cl -c $M_client where $M_root/$from_src | grep -v "^-" | sed -e 's! .*!!'`;
	chop $mapto;

	$bview = "$mapto $from_loc";
	$file = $mapto;

	return ($bview,$file);
}

# map any depot location to
# a file in the client view
sub map_to_clientview {
	local($from_loc, @cview) = @_;

	if( $from_loc =~ /\/javabase\// )
	{
		$base = "javabase";
	}
	else
	{
		$base = "base";
	}

	# This integration method only works for
	# files under base/... so get rid of all
	# others, eg. conf/... etc.
	if( $from_loc !~ /.*\/$base\/.*/ ) {
		return &map_to_clientview_src_not_base;
	}

	## find the module name
	$from_mod = $from_loc;
	($from_mod) =~ s!.*/$base/([^/]+)/.*!\1!;

	## now tear off any after base/module/[module_X.X/]
	$from_mod2 = $from_loc;
	($from_mod2) =~ s!/$base/$from_mod(((/$from_mod)?_[^/]+)?)/.*!/$base/$from_mod\1/!;

	$path = $from_loc;
	($path) =~ s!$from_mod2!!;

	# filename mapped to local directory structure
	$file = "$M_root/$base/$from_mod/$path";

	($path) =~ s![^/]*$!\.\.\.!;
	($from_loc) =~ s![^/]*$!\.\.\.!;

	$bview = "";

	# try to map module
	foreach(@cview) {
		($to_mod) = m!^\t([^ ]*/$base/$from_mod((/$from_mod)?_[^/]+)?/)!;
		if($to_mod ne "") {
			$bview = "$to_mod$path $from_loc";
			goto endloop;
		}
	}
	# can't map module, so just use base
	foreach(@cview) {
		($to_base) = m!^\t([^ ]*/$base/)\.\.\.!;
		if($to_base ne "") {
			$bview = "$to_base$from_mod/$path $from_loc";
			($bview) =~ s!///[^ ]+!/\.\.\.!;
			goto endloop;
		}
	}

	endloop:

	return ($bview,$file);
}

#
# Function to integrate a list, given as a string,
# using the given branch
sub integrate_list_str {
	local ($int_opts, $branch_view, $list_str) = @_;

	my @hack_list = ();
	($jobno, $summary) = choose_job_for_command $M_user, \@hack_list;

	$change = get_change_for_job $jobno;
	if ($change == 0) {
		create_change_for_job $jobno, $summary, $M_user, $M_client;
		$change = get_change_for_job $jobno;
	}

	print "Integrating file(s)...\n";
	print "\n\n[\np4cl -c $M_client integrate $int_opts -r -c $change -b $branch_name $list_str\n]\n\n" ;
	system( "p4cl -c $M_client integrate $int_opts -r -c $change -b $branch_name $list_str" );
}

#
# create a new temporary branch for integration
sub create_branch {
	local(@list) = @_;

	@clientview = &get_client_view;

	foreach(@list) {
		# for each selected file,
		# find where to integrate it to on the client
		($bv,$file) = &map_to_clientview($_, @clientview);
		if( $bv ne "" ) {
			# create the branch view for each selection
			push @branch_view, $bv if ($bv !~ m!\/\/\/!);
		}
		push @files, $file;
	}
	%seen = ();
	foreach $item (@branch_view) {
		$seen{$item}++;
	}
	@branch_view = keys %seen;

	$branch_name = "$M_user.integrate";

	open( P4_BRANCH, "| p4cl branch -i") || die ("Unable to create branch spec for integration!\n");
	print P4_BRANCH <<Here;
# A Perforce Branch Specification.
#
#  Branch:      The branch name.
#  Update:      The date this specification was last modified.
#  Access:      The date of the last 'integrate' using this branch.
#  Owner:       The user who created this branch.
#  Description: A short description of the branch (optional).
#  Options:     Branch update options: locked or unlocked.
#  View:        Lines to map source depot files to target depot files.
#
# Use 'p4 help branch' to see more about branch views.

Branch:	$branch_name

Description:
	$descr

Options:	unlocked

View:
Here
	foreach(@branch_view) {
		print P4_BRANCH "\t$_\n";
	}
	print P4_BRANCH "\n";
	close P4_BRANCH;

	return ($branch_name, @files);
}

#
# Function to integrate a given change (or range of changes)
# to the current client view
sub integrate_fix {
	local (@args) = @_;
	@int_opts = ();
	$for_user = "all";

	while ($#args > 0 and $args[0] =~ /^-/) {
		if ($args[0] =~ /^-u/) {
			if( $args[0] eq "-u" ) {
				$for_user = $args[1];
				shift(@args);
			}
			else {
				$for_user = $args[0];
				($for_user) =~ s!^-u!!;
			}
			shift(@args);
		}
		else {
			push @int_opts, $args[0];
			shift(@args);
		}
	}

	if ($#args == -1) {
		print "Expected change number to integrate!\n";
		exit;
	}

	if( $for_user eq "" ) {
		print "Expected -u <user>\n";
		exit;
	}

	$change_arg = $args[0];

	@range = split(',', $change_arg);
	if($#range == 0) {
		$changerange = "$change_arg,$change_arg";
	}
	elsif($#range > 1) {
		die "Invalid change range";
	}
	else {
		$changerange = $change_arg;
	}

	@list = &get_files_for_changerange($changerange, $for_user);

	($branch_name, @files) = &create_branch(@list);

	&integrate_list_str("@int_opts", $branch_name, "...\@$changerange");
	system("p4cl branch -d $branch_name");
}

sub get_files_for_changerange
{
	my $change_arg = $_[0];
	my $for_user = $_[1];
	my @AffectedFiles = ();

	@range = split(',', $change_arg);
	@change_list = ();

	for($c = $range[0]; $c <= $range[1]; $c++) {
		push @change_list, $c;
	}

	foreach $change (@change_list)
	{
		# Use P4 to get list of files affected by this changelist
		open(P4OUTPUT, "p4cl describe -s $change 2>&1 | grep \"^[N\.]\" |")
			or die "Unable to describe change $change";

		while (<P4OUTPUT>) {
			chomp;
			if( $_ !~ m!^\.\.\.! and $for_user ne "all" and $_ !~ m!by $for_user! ) {
				goto next_change;
			}
			next if ($_ !~ m!^\.\.\.!);
			$affected = $_;
			($affected) =~ s!\.\.\. ([^#]*).*!\1!;
			if ($affected ne "" and $affected ne $_) {
				push @AffectedFiles, $affected;
			}
		}
		next_change:
		close P4OUTPUT;
	}

	return @AffectedFiles;
}

sub get_client_view {
	@clientview = ();
	open (CLIENT, "p4cl -c $M_client client -o |") || die "Unable to run p4";
	$gotView = 0;
	while (<CLIENT>) {
		next if ($_ !~ m!^View! and $gotView eq 0);
		$gotView = 1;
		next if ($_ =~ m!^View!);
		push @clientview, $_;
	}
	return @clientview;
}	

