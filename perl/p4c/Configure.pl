#!/usr/bin/env perl
use strict;
use warnings;

use Getopt::Long;
use File::Copy;
use File::Path qw(rmtree mkpath);
use File::Basename;
use File::Spec::Functions;
use Cwd qw(abs_path cwd);
use Pod::Usage;
use Hash::Util qw(lock_hash lock_keys);

my %conf = ( name      => 'p4c'
	   , version   => 0.4
	   , tar       => 'tar'
	   , tarflags  => '-cvf'
	   , gzip      => 'gzip'
	   , gzipflags => '-6'
	   );
lock_hash %conf;
my %cmds =
 ( extcmd => [ 'annotate'    , 'grep'         , 'resolve'
	     , 'archive'     , 'have'         , 'resolved'
	     , 'change'      , 'info'         , 'restore'
	     , 'changes'     , 'intgrated'    , 'reviews'
	     , 'changelist'  , 'interchanges' , 'shelve'
	     , 'changelists' , 'istat'        , 'sizes'
	     , 'cstat'       , 'jobs'         , 'sync'
	     , 'diff'        , 'labels'       , 'tag'
	     , 'diff2'       , 'labsync'      , 'unshelve'
	     , 'filelog'     , 'obliterate'   , 'update'
	     , 'files'       , 'opened'       , 'verify'
	     , 'fixes'       , 'print'        , 'where'
	     , 'flush'       , 'protects'
	     , 'fstat'       , 'reopen'
	     ]
 , merge  => ['copy', 'integrate']
 , move   => ['rename']
 , client => ['workspace']
 , add    => []
 , delete => []
 , dirs   => []
 , edit   => []
 , lock   => []
 , revert => []
 , submit => []
 , unlock => []
 , jira        => []
 , 'open-job'  => []
 , 'close-job' => []
 , 'my-files'  => ['my-changes', 'my-jobs', 'my-groups']
 , 'merge-fix' => []
 );
lock_hash %cmds;
my %scripts = ( 'p4c.pl' => ['p4c'] );
lock_hash %scripts;
my %pkgs = ( 'P4Ctx.pkg.pm'         => 'P4Ctx.pm'
	   , 'P4CtxJobPrint.pkg.pm' => 'P4CtxJobPrint.pm'
	   , 'P4CtxUtils.pkg.pm'    => 'P4CtxUtils.pm'
	   );
lock_hash %pkgs;
my %defines = ( P4CLBIN      => "p4cl"
	      , P4SERVERCONN => "peforce:1666"
	      );
lock_keys %defines;

umask 0002;
umask 0022 unless $>;

sub cmd_name ($) { 'p4c-' . (shift) . '.pl'     }
sub dist_name () { "$conf{name}-$conf{version}" }

sub print_exec ($) {
    my $cmd = shift;
    print "    $cmd\n";
    0 == system $cmd or die "Can't exec $cmd: $!\n";
}

sub lock_configs (\%\%) {
    my ($configs, $defines) = @_;
    foreach (keys %$defines) {
	unless (exists $configs->{$_}) {
	    warn "Unknown define [$_], ignoring\n";
	    next;
	}
	$configs->{$_} = $defines->{$_};
    }
    lock_hash %$configs;
    $configs;
}

sub move_dist () {
    my $fullname = abs_path $0;
    my ($name, $path) = fileparse $fullname;
    unless ((abs_path cwd) eq (abs_path $path)) {
	    print "$name runs inside the distribution directory\n";
	    print "    cd $path\n";
	    chdir $path or die "Can't cd to $path: $!\n";
    }
    $name, $path, $fullname;
}

sub clean_dir ($) {
    my $dir = shift;
    if (-d $dir) {
	print "    rmdir $dir\n";
	rmtree $dir or die "Can't remove $dir: $!\n";
    }
}

sub clean_dist ($) {
    my $dist = shift;
    clean_dir $dist;
    my $distcheck = sub ($) { my $pkg = shift;
			      if (-e $pkg) {
				 print "    rm $pkg\n";
				 unlink $pkg or die "Can't rm old $pkg: $!\n";
			      }
		    };
    map { $distcheck->($_) } ("$dist.tar", "$dist.tar.gz");
}

sub mkclean () {
    print "Clean distribution directory...\n";
    clean_dist dist_name;
    foreach (keys %pkgs) {
	next unless -e $pkgs{$_};
	print "    rm $pkgs{$_}\n";
	unlink $pkgs{$_} or warn "Can't remove $pkgs{$_}: $!\n";
    }
}

sub copy_file ($$) {
    my ($file, $newfile) = @_;
    print "    cp $file $newfile\n";
    copy $file, $newfile or die "Can't cp $newfile: $!\n";
}

sub copy_bin ($$) {
    my ($bin, $newbin) = @_;
    copy_file $bin, $newbin;
    my $mode = 0777 - umask;
    printf "    chmod 0%03o $newbin\n", $mode;
    chmod $mode, $newbin or die "Can't chmod $newbin: $!\n";
}

sub mkdist ($) {
    my $name = shift;
    my $dist = dist_name;
    clean_dist $dist;
    print "    mkdir $dist\n";
    mkdir $dist or die "Can't create $dist: $!\n";
    my @files = ('README', keys %pkgs);
    my @bins = ($name, keys %scripts, map {cmd_name $_} (keys %cmds));
    copy_file $_, (catfile $dist, $_) or die "Can't cp: $!\n" foreach (@files);
    copy_bin  $_, (catfile $dist, $_) or die "Can't cp: $!\n" foreach (@bins);
    my $tarcmd = "$conf{tar} $conf{tarflags} $dist.tar $dist";
    my $gzipcmd = "$conf{gzip} $conf{gzipflags} $dist.tar";
    map { print_exec $_ } ($tarcmd, $gzipcmd);
}

sub subdefs ($) {
    my $line = shift;
    $line =~ s/$_/$defines{$_}/g foreach (keys %defines);
    $line;
}

sub mkbuild () {
    foreach (keys %pkgs) {
	my $outfile = $pkgs{$_};
	print "Building $pkgs{$_}\n";
	open my $file, '<', $_       or die "Can't open $_: $!\n";
	open my $out,  '>', $outfile or die "Can't write $outfile: $!\n";
	print $out subdefs $_ while <$file>;
	close $file;
	close $out;
    }
}

sub not_built_yet ($) {
    my $name = shift;
    my $dist = dist_name;
    foreach (keys %pkgs) {
	die "$dist was never built!  Please run $name --make to build it.\n"
	    unless -e $pkgs{$_};
    }
}

sub mktest () {
    my $v = 5.008001;
    print "In an ideal world this would test the p4c scripts, because in an\n"
        . "ideal world programmers would be allowed time to write tests.\n"
        . "Unfortunately we do not live in an ideal world and therefore\n"
        . "this script test only that the right version of Perl is present.\n"
        . "Perl version needed is $v, the current version is $]\n\n";
    if ($v <= $]) { print "And it's OK, you can run the scripts.\n"       }
    else          { die "I will fail!  You better update Perl buddy.\n" }
}

sub mkslinks ($$$;$) {
    my ($name, $bindir, $links, $nmproc) = @_;
    my $fname = $nmproc ? $nmproc->($name) : $name;
    foreach (@$links) {
	my $linkname = $nmproc ? $nmproc->($_) : $_;
	my $link = catfile $bindir, $linkname;
	print "    ln -s $fname $link\n";
	symlink $fname, $link or die "Can't softlink $link to $fname: $!\n";
    }
}

sub mkinstall ($$$) {
    my ($confname, $prefix, $bindir) = @_;
    not_built_yet $confname;
    foreach ($prefix, $bindir) {
	print "Check if $_ exists\n";
	unless (-d $_) {
	    print "    mkdir $_\n";
	    mkpath $_;
	}
	print "OK, $_ exists\n";
    }
    copy_file $_, (catfile $bindir, $_) foreach (map {$pkgs{$_}} keys %pkgs);
    copy_bin  $_, (catfile $bindir, $_) foreach (keys %scripts);
    copy_bin  $_, (catfile $bindir, $_) foreach (map {cmd_name $_} keys %cmds);
    mkslinks  $_, $bindir, $scripts{$_}            foreach (keys %scripts);
    mkslinks  $_, $bindir, $cmds{$_},   \&cmd_name foreach (keys %cmds);
}

sub prefix (@) {
    my $prefix = '/usr/local';
    my ($bindir, $cmd, %defs);
    my $errmsg = sub { die "Sorry, you cannot mix commands\n" };
    GetOptions( 'prefix=s' => \$prefix
	      , 'bindir=s' => \$bindir
	      , 'define=s' => \%defs
	      , 'help'     => sub { $cmd = 'help' }
	      , 'clean'    => sub { $errmsg->() if $cmd; $cmd = 'clean'   }
	      , 'dist'     => sub { $errmsg->() if $cmd; $cmd = 'dist'    }
	      , 'make'     => sub { $errmsg->() if $cmd; $cmd = 'make'    }
	      , 'test'     => sub { $errmsg->() if $cmd; $cmd = 'test'    }
	      , 'install'  => sub { $errmsg->() if $cmd; $cmd = 'install' }
	      ) or pod2usage(-verbose => 0, -output => \*STDERR);
    lock_configs %defines, %defs;
    my ($name, $path, $fullname) = move_dist;
    my %cmds = ( help    => sub { pod2usage(-verbose => 1, -exitval => 0,
					    -input => $fullname         ) }
	       , clean   => sub { mkclean                                 }
	       , dist    => sub { mkdist $name                            }
	       , make    => sub { mkbuild                                 }
	       , test    => sub { mktest                                  }
	       , install => sub { mkinstall $name, $prefix,
					    ($bindir || "$prefix/bin")    }
	       );
    $cmd = 'help' unless $cmd and exists $cmds{$cmd};
    $cmds{$cmd}->();
}

prefix @ARGV;

__END__

=head1 NAME

Configure.pl - Configuration and install script for p4c.pl

=head1 SYNOPSIS

 Configure.pl --clean [--define key=val]
 Configure.pl --dist  [--define key=val]
 Configure.pl --make  [--define key=val]
 Configure.pl --test  [--define key=val]
 Configure.pl --install [-p dir|--prefix dir] [-b dir|--bindir dir]
              [--define key=val]

=cut

