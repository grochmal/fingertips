#!/usr/bin/env perl
use strict;
use warnings;

use File::Basename;
use Cwd qw(abs_path);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf parse_cmd);

sub check_revert {
	my $client = shift;
	my $change_no;

	shift(@ARGV);
	open( P4_OUT, "p4cl -c $client opened @ARGV |");
	my $line = <P4_OUT>;
	close( P4_OUT );
	my @fields = split(/ /, $line);
	# Make sure this is a proper description
	if ($fields[3] eq "change") {
		$change_no = $fields[4];
	}

	# Do the revert
	system( "p4cl -c $client revert @ARGV" );

	# Delete the change if there are no files left	
	if ($change_no) {
		# Check if files in the change still exist
		open( P4_OUT,"p4cl -c $client opened 2>/dev/null | " . 
						"grep \"change $change_no\" |");
		$line = <P4_OUT>;
		close( P4_OUT );
		if (!$line) {
			&delete_change($change_no);
		}
	}
}

print "Not implemented yet!\n";

