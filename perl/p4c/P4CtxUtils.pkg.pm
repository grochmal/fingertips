#!/usr/bin/perl
package P4CtxUtils;

use strict;
use warnings;

use Carp;

BEGIN {
    require Exporter;
    our $VERSION   = 0.1;
    our @ISA       = qw(Exporter);
    our @EXPORT_OK = qw(disallow_in_build);
}

sub disallow_in_build ($$) {
    my $croot = shift;
    my $cmd = shift;
    croak "You shan't use $cmd in the backing tree build, you may lose your\n"
	. "code if you do!\n" if $croot =~ m{^/cortex/build/};
}

sub set_job_state ($$@) {
    use strict;
    use warnings;
    my $state = shift;
    my $user = shift;
    while (my $job = shift) {
	open my $p4_job_out, "p4cl job -o $job |";
	my @form = <$p4_job_out>;
	close $p4_job_out;
	@form = map {s/^Status:.*/Status:\t\Q$state\E/;
		     s/^Assignee:.*/Assignee:\t\Q$user\E/;
		     $_} @form;
	open my $p4_job_in, "| p4cl job -i";
	print $p4_job_in @form;
	close $p4_job_in;
    }
}

=head2 user_report($user, $what)

Prints a report on:

    files currently opened by the user,

    changes pending by the user,

    jobs assigned (and open) to the user,

    groups the user is part of.

A list of lines is assigned to the report as soon as we
can decide for 'what' the report is for.  Then these
lines are filtered by 'user' to get the required output.

The @rpt array shall always be populated (possibly by an
empty list) unless we hit the die statement in the case
of an unrecognized 'what' for the report.

=cut
sub user_report ($$) {
    use strict;
    use warnings;
    my ($user, $what) = @_;
    my @rpt;
    if ("files" eq $what) {
        @rpt = grep {m#by $user@#} `p4cl opened -a`;
    }
    elsif ("changes" eq $what) {
        @rpt = grep {m#by $user@#} `p4cl changes -s pending`;
    }
    elsif ("jobs" eq $what) {
        @rpt = grep {not m#^NMR#} `p4cl jobs -e 'status=open assignee=$user'`;
    }
    elsif ("groups" eq $what) {
        @rpt = `p4 groups $user`;
    }
    else {
        croak "Unsupported user report on [$what]\n";
    }
    print @rpt;
}

sub delete_change {
	my $change_no = shift;
	my $client = shift;
	my @lines = `p4cl -c $client fixes | grep "change $change_no"`;
	foreach (@lines)
	{
		my $jobno = $_;
		$jobno =~ s/ .*//;
		chop $jobno;

		# break connection between change and jobno
		system( "p4cl -c $client fix -d -c $change_no $jobno");
	}

	# delete the change
	system( "p4cl -c $client change -d $change_no");
}

1;

