#!/usr/bin/perl
package P4CtxJobPrint;

use strict;
use warnings;

use Carp;

BEGIN {
    require Exporter;
    our $VERSION   = 0.2;
    our @ISA       = qw(Exporter);
    our @EXPORT_OK = qw(add2job jobs4change changes4job pending_changes
			get_jobs);
}

sub set_job_state ($$$);  ## TODO from another module

sub print_choices (\%\@) {
    my ($jobs, $keys) = @_;
    my $digits = scalar @$keys;
    my $dnum = 1;
    $dnum++ until 1 > ($digits /= 10);
    my $i = 1;
    my $map = {};
    print "\n";
    print "Ops, nothing to choose, check your jobs and filter\n" unless @$keys;
    foreach (@$keys) {
	printf "[%*d] %-18s $jobs->{$_}->{desc}\n", $dnum, $i, $_;
	$map->{$i} = $_;
	$i++;
    }
    \%$map;
}

sub dtnum ($) { my $dtnum = shift; $dtnum =~ s{/}{}g; $dtnum }

sub get_jobs ($) {
    my $conf = shift;
    my $cmd = "p4cl -s jobs -e 'assignee=$conf->{user}'";
    print "Checking for all your jobs [$cmd]\n";
    my %jobs = map { my ($nm, $dt, $st, $desc) =
		     /^info:\s(\S+)\son\s(\S+)\sby\s\S+\s\*(\S+)\*\s'(.*)'/;
		     $nm => { date => $dt, status => $st, desc => $desc } }
	       grep { /^info:\s/ and not /^info:\sNMR/ } `$cmd`;
    my @sorted = sort { dtnum $jobs{$a}->{date} <=> dtnum $jobs{$b}->{date} }
		 keys %jobs;
    my @open   = grep { $jobs{$_}->{status} =~ /open/   } @sorted;
    my @closed = grep { $jobs{$_}->{status} =~ /closed/ } @sorted;
    return \%jobs, \@sorted, \@open, \@closed;
}

sub get_choice (\%$) {
    my ($choices, $prompt) = @_;
    my $choice;
    my $filter;
    until (exists $choices->{$choice}) {
	my $map = print_choices %$choices, $filter;
	print "\nType 'more' for more options or 'grep <Pattern> to search\n";
	print "$prompt > ";
	while (<STDIN>) {
	    chomp;
	    if (exists $map->{$_}) {
		$choice = $map->{$_};
		last;
	    }
	    return undef if 'more' eq $_;
	    if (/^grep\s+(.*)/) {
		$filter = $1;
		$filter =~ s/\s*$//;
		last;
	    }
	    $filter = undef;
	    last if /^$/;
	    print "\nInvalid option! Try again...\n > ";
	}
    }
    return $choice;
}

sub choose_job_for_command ($) {
    my $conf = shift;
    my ($jobs, $all, $open, $closed) = get_jobs $conf;
    my ($stat, $map, @choices) = ('open');
    my %actions = { more => sub { 'open' == $stat ? $choices = $open
					 : $choices = $closed;
				  $map = print_choices $jobs, $choices }
		  , grep => sub { 
    until ($job) {
	$map = print_choices %$jobs, @choices;
	$_ = <STDIN>;
	chomp;
	unless ($_)            { print $msg; next }
	if (exists $map->{$_}) { $job = $map->{$_} }
	if (/^more$/)          { $stat = $next_stat->{$stat} }
	if (/^grep\s+(.*)/)    {}
    }

    my $stat = 'open'; # open, closed, suspended
    my ($jobs, $job);
    my $swing_stat = sub ($) {
			 my $stat = shift;
			 my $map = {open   => 'closed',
			            closed => 'open'  };
			 croak "$stat isn't a job status"
			       unless exists $map->{$stat};
			 return $map->{$stat};
		     };
    until ($job) {
	$jobs = get_jobs $stat; #, $user;
	$job = get_choice %$jobs, "Enter the job to assign these file(s) to";
	$stat = $swing_stat->($stat);
    }
    set_job_state 'open', $user, $job;
    return $job, $jobs->{$job};
}

sub check_given_job ($$) {
    use strict;
    use warnings;
    my ($user, $ctx_flags) = @_;
    my $job = have_job_option $ctx_flags;
    return undef, undef unless $job;
    my $open_jobs = get_jobs 'open'; #, $user;
    return $job, $open_jobs->{$job} if exists $open_jobs->{$job};
    my $closed_jobs = get_jobs 'closed'; #, $user;
    if (exists $closed_jobs->{$job}) {
	set_job_state 'open', $user, $job;
	return $job, $closed_jobs->{$job};
    }
    warn "Job [$job] do not exist in p4, please select a job from the list\n";
    return undef, undef;
}

sub get_change_for_job ($) {
    use strict;
    use warnings;
    my $jobno = shift;
    my $change;
    open my $p4_fixes, "p4cl -s fixes -j $jobno |";
    while (<$p4_fixes>) {
	next if /^exit/;
	my @fields = split;
	$change = $fields[5];
	open my $p4_describe, "p4cl -s describe -s $change |";
	my $line = <$p4_describe>;
	close $p4_describe;
	last if $line =~ /\*pending\*/;
	$change = undef;
    }
    close $p4_fixes;
    return $change;
}

sub create_change_for_job ($$$$) {
    use strict;
    use warnings;
    my ($job, $summary, $user, $client) = @_;
    print "Creating change for [$job] [$summary] for [$user] on [$client]\n";
    system "p4cl -c $client change -i <<!!
Change:	new
Client:	$client
User:	$user
Status:	new
Description:
	SUMMARY: $summary
	DETAIL:
Jobs:	$job
!!";
    set_job_state "open", $user, $job;
}

sub jobs4change ($$) {
    my ($conf, $change) = @_;
    my @fixes = `$conf->{bin} -s fixes -c $change`;
    map { /^info:\s(\S+)\sfixed\sby\schange\s$change/ } @fixes;
}

sub changes4job ($$) {
    my ($conf, $job) = @_;
    my @fixes = `$conf->{bin} -s fixes -j $job`;
    map { /^info:\s$job\sfixed\sby\schange\s(\S+)\s/ } @fixes;
}

sub pending_changes ($) {
    my $conf = shift;
    my $cmd = "$conf->{bin} -s changes -s pending -u $conf->{user}";
    print "Check pending changelists [$cmd]\n";
    map { /^info:\sChange\s(\S+)\son/ } `$cmd`;
}

# FIXME only one pending change can exist for a job.
# as the client on which we are working is not taken into
# consideration
sub jobs_in_progress ($) {
    my $conf = shift;
my $user;
    my @chgs = `$conf->{bin} changes -s pending -u $conf->{user}`;
    croak "No pending changes for $user, abort\n" unless @chgs;
    my %changes = ();
    my %jobs = ();
    foreach (@chgs) {
	my @fields = split;
	my $changeno = $fields[1];
	open my $p4fixes, "p4cl fixes -c $changeno |";
	while (<$p4fixes>) {
	    my @fixfields = split;
	    my $job = $fixfields[0];
	    my $desc = "No description given.";
	    open my $p4cmd, "p4cl jobs -e $job |";
	    while (<$p4cmd>) {
		if (/'(.*)'$/) {
		    $desc = $1;
		last;
		}
	    close $p4cmd;
	    }
	    $jobs{$job} = $desc;
	    $changes{$job} = $changeno;
	}
	close $p4fixes;
    }
    return \%jobs, \%changes;
}

sub add_file_to_job ($$$) {
    use strict;
    use warnings;
    my ($user, $ctx_flags, $client) = @_;
    my ($job, $summary) = choose_job_for_command $user, $ctx_flags;
    my $change = get_change_for_job $job;
    until ($change) {
	create_change_for_job $job, $summary, $user, $client;
	$change = get_change_for_job $job;
    }
    return $change;
}

sub add2job ($;$) {
    my ($conf, $jobname) = @_;
    undef;
}

1;

