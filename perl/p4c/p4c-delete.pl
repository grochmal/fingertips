#!/usr/bin/env perl
use strict;
use warnings;

use File::Basename;
use Cwd qw(abs_path);
BEGIN { push @INC, dirname abs_path $0 };
use P4Ctx qw(get_conf parse_cmd);

sub disallow_in_build ($);  #TODO
sub add_file_to_job ($$$); #TODO

sub add_file_delete ($$$$$$$) {
    use strict;
    use warnings;
    my ($ctx_flags, $g_flags, $cmd, $args, $user, $pwd, $client) = @_;
    disallow_in_build $pwd;
    print "\n*** You are deleting one or more files. ***\n";
    my $change = add_file_to_job $user, $ctx_flags, $client;
    print "\nDeleting file(s)...\n";
    my $p4cmd = "p4cl @$g_flags -c $client delete -c $change @$args";
    print "$p4cmd\n";
    system $p4cmd;
}

print "Not implemented yet!\n";

