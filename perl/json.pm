#!/usr/bin/env perl
#package JSON;
# based on Makamaka Hannyaharamitu's JSON:PP,
# removed utf-8 processing to gain spped

use strict;
use warnings;

use Carp;
use Data::Dumper;

BEGIN {
    require Exporter;
    our $VERSION   = 0.1;
    our @ISA       = qw(Exporter);
    our @EXPORT_OK = qw(decode_json);
}

my $test = <<EOF
{
"abstract" : "JSON (JavaScript Object Notation) encoder/decoder"
,
"author" : [ "Makamaka Hannyaharamitu, E<lt>makamaka[at]cpan.orgE<gt>" ]
,
"dynamic_config" : 1
,
"generated_by" :
  "ExtUtils::MakeMaker version 6.62, CPAN::Meta::Converter version 2.130880"
,
"license" : [ "perl_5", "perl_5.8" ]
,
"True" : true
,
"Null" : null
,
"False" : false
,
"meta-spec" : {
              "url" : "http://search.cpan.org/perldoc?CPAN::Meta::Spec"
              ,
              "version" : "2"
              }
,
"name" : "JSON"
,
"no_index" : { "directory" : [ "t", "inc" ] }
,
"prereqs" : {
            "build" : { "requires" : { "ExtUtils::MakeMaker" : "0" } }
            ,
            "configure" : { "requires" : { "ExtUtils::MakeMaker" : "0" } }
            ,
            "runtime" : {
                        "recommends" : { "JSON::XS" : "2.34" }
                        ,
                        "requires"   : { "Test::More" : "0" }
                        }
            }
,
"release_status" : "stable"
,
"resources" : { "repository" : { "url" : "https://github.com/makamaka/JSON" } }
,
"version" : "2.59"
}

EOF
;

my %escapes = ( b    => "\x8"
	      , t    => "\x9"
	      , n    => "\xA"
	      , f    => "\xC"
	      , r    => "\xD"
	      , '\\' => '\\'
	      , '"'  => '"'
	      , '/'  => '/'
	      );

# shamelessly copied and modified from JSON::XS code.
sub json_true  () { bless \(my $dummy = 1), "json_bool" }
sub json_false () { bless \(my $dummy = 0), "json_bool" }
sub json_null  () { undef                               }

sub decode_error ($$$) {
    my ($txt, $at, $msg) = @_;
    my $str = defined $txt ? substr $txt, $at : '';
    my $mess   = '';
    foreach my $c (unpack 'U*', $str) { # emulate pv_uni_display() ?
	$mess .= $c == 0x07 ? '\a'
	       : $c == 0x09 ? '\t'
	       : $c == 0x0a ? '\n'
	       : $c == 0x0d ? '\r'
	       : $c == 0x0c ? '\f'
	       : $c <  0x20 ? sprintf '\x{%02x}', $c
	       : $c == 0x5c ? '\\\\'
	       : $c <  0x80 ? chr $c
	       : sprintf '\x{%02x}', $c
	       ;
	if (length $mess >= 20) {
	    $mess .= '...';
	    last;
	}
    }
    $mess = 'EOF' unless length $mess;
    die "$msg, at character offset $at (before (( $mess  )) )";
}

sub value ($$$$);

sub next_chr ($$$) {
    my ($txt, $len, $at) = @_;
    return $at if $at >= $len;
    $at + 1, substr $txt, $at, 1;
}

sub white ($$$$) {
    my ($txt, $len, $at, $ch) = @_;
    ($at, $ch) = next_chr $txt, $len, $at while defined $ch and $ch le ' ';
    $at, $ch;
}

sub string ($$$$;$) {
    my ($txt, $len, $at, $ch, $s) = @_;
    $s = '' unless $s;
    ($at, $ch) = next_chr $txt, $len, $at;
    while (defined $ch) {
	if ('"' eq $ch) {
	    ($at, $ch) = next_chr $txt, $len, $at;
	    return $at, $ch, $s;
	}
	elsif ('\\' eq $ch) {
	    ($at, $ch) = next_chr $txt, $len, $at;
	    if (exists $escapes{$ch}) {
		$s .= $escapes{$ch};
	    }
	    elsif ('u' eq $ch) { # UNICODE handling
		my $u = '';
		foreach (1..4) {
		    ($at, $ch) = next_chr $txt, $len, $at;
		    decode_error $txt, $at,
				 "bad unicode escape" if $ch !~ /[0-9a-fA-F]/;
		    $u .= $ch;
		}
		$s .= "\\u$u";
	    }
	    else {
		$at -= 2;
		decode_error $txt, $at, "illegal backslash sequence in string";
	    }
	}
	else {
	    $s .= $ch;
	}
	($at, $ch) = next_chr $txt, $len, $at;
    }
    decode_error $txt, $at, "unexpected end of file while parsing JSON string";
}

sub object ($$$$;$) {
    my ($txt, $len, $at, $ch, $o) = @_;
    $o = {} unless $o;
    ($at, $ch) = next_chr $txt , $len, $at;
    ($at, $ch) = white $txt, $len, $at, $ch;
    if (defined $ch and '}' eq $ch) {
	($at, $ch) = next_chr $txt , $len, $at;
	return $at, $ch, $o;
    }
    while (defined $ch) {
	my $k;
	decode_error $txt, $at, "hash key must be a string" unless '"' eq $ch;
	($at, $ch, $k) = string $txt, $len, $at, $ch;
	($at, $ch) = white $txt, $len, $at, $ch;
	if (not defined $ch or ':' ne $ch) {
	    $at -= 1;
	    decode_error $txt, $at, "':' expected";
	}
	($at, $ch)           = next_chr $txt , $len, $at;
	($at, $ch, $o->{$k}) = value $txt, $len, $at, $ch;
	($at, $ch)           = white $txt, $len, $at, $ch;
	last if not defined $ch;
	if ('}' eq $ch) {
	    ($at, $ch) = next_chr $txt , $len, $at;
	    return $at, $ch, $o;
	}
	last if ',' ne $ch;
	($at, $ch) = next_chr $txt , $len, $at;
	($at, $ch) = white $txt, $len, $at, $ch;
    }
    $at -= 1;
    decode_error $txt, $at, ", or } expected while parsing object/hash";
}

sub array ($$$$;$) {
    my ($txt, $len, $at, $ch, $a) = @_;
    $a = [] unless $a;
    ($at, $ch) = next_chr $txt , $len, $at;
    ($at, $ch) = white $txt, $len, $at, $ch;
    if (defined $ch and ']' eq $ch) {
	($at, $ch) = next_chr $txt , $len, $at;
	return $at, $ch, $a;
    }
    while (defined $ch) {
	my $elem;
	($at, $ch, $elem) = value $txt, $len, $at, $ch;
	push @$a, $elem;
	($at, $ch) = white $txt, $len, $at, $ch;
	last if not defined $ch;
	if (']' eq $ch) {
	    ($at, $ch) = next_chr $txt , $len, $at;
	    return $at, $ch, $a;
	}
	last if ',' ne $ch;
	($at, $ch) = next_chr $txt , $len, $at;
	($at, $ch) = white $txt, $len, $at, $ch;
    }
    decode_error $txt, $at, ", or ] expected while parsing array";
}

sub word ($$$$) {
    my ($txt, $len, $at, $ch) = @_;
    my $word =  substr $txt, $at - 1, 4;
    if ('true' eq $word) {
	$at += 3;
	($at, $ch) = next_chr $txt , $len, $at;
	return $at, $ch, (json_true);
    }
    elsif ('null' eq $word) {
	$at += 3;
	($at, $ch) = next_chr $txt , $len, $at;
	return $at, $ch, (json_null);
    }
    elsif ('fals' eq $word) {
	$at += 3;
	if ('e' eq substr $txt, $at, 1) {
	    $at += 1;
	    ($at, $ch) = next_chr $txt , $len, $at;
	    return $at, $ch, (json_false);
	}
    }
    $at -= 1;
    decode_error $txt, $at, "'null' expected"  if $word =~ /^n/;
    decode_error $txt, $at, "'true' expected"  if $word =~ /^t/;
    decode_error $txt, $at, "'false' expected" if $word =~ /^f/;
    decode_error $txt, $at, "malformed JSON, "
			  . "neither array, object, number, string or atom";
}

sub number ($$$$) {
    my ($txt, $len, $at, $ch) = @_;
    my $n    = '';
    if ('-' eq $ch) {
	$n = '-';
	($at, $ch) = next_chr $txt , $len, $at;
	decode_error $txt, $at,
		     "malformed number (no digits after initial minus)"
		     if not defined $ch or $ch !~ /\d/;
    }
    decode_error $txt, $at, "malformed number (leading zero)" if '0' eq $ch;
    while (defined $ch and $ch =~ /\d/) {
	$n .= $ch;
	($at, $ch) = next_chr $txt , $len, $at;;
    }
    if (defined $ch and '.' eq $ch) {
	$n .= '.';
	($at, $ch) = next_chr $txt , $len, $at;
	decode_error $txt, $at,
		     "malformed number (no digits after decimal point)"
		     if not defined $ch or $ch !~ /\d/;
	$n .= $ch;
	($at, $ch) = next_chr $txt , $len, $at;
	while (defined $ch and $ch =~ /\d/) {
	    $n .= $ch;
	    ($at, $ch) = next_chr $txt , $len, $at;
	}
    }
    if (defined $ch and ('e' eq $ch or 'E' eq $ch)) {
	$n .= $ch;
	($at, $ch) = next_chr $txt , $len, $at;
	if (defined $ch and ('+' eq $ch or '-' eq $ch)) {
	    $n .= $ch;
	    ($at, $ch) = next_chr $txt , $len, $at;
	}
	decode_error $txt, $at, "malformed number (no digits after exp sign)"
		     if (not defined $ch or $ch !~ /\d/);
	$n .= $ch;
	while (defined $ch and $ch =~ /\d/) {
	    $n .= $ch;
	    ($at, $ch) = next_chr $txt , $len, $at;
	}
    }
    return $at, $ch, 0 + $n;
}

sub value ($$$$) {
    my ($txt, $len, $at, $ch) = @_;
    ($at, $ch) = white $txt, $len, $at, $ch;
    return $at, undef, undef           if not defined $ch;
    return object $txt, $len, $at, $ch if $ch eq '{';
    return array  $txt, $len, $at, $ch if $ch eq '[';
    return string $txt, $len, $at, $ch if $ch eq '"';
    return number $txt, $len, $at, $ch if $ch =~ /[0-9]/ or $ch eq '-';
    return word   $txt, $len, $at, $ch;
}

sub decode_json ($) {
    my ($txt, $at, $ch, $result) = (shift, 0);
    my $bad_json = "malformed JSON string, neither array,"
		 . " object, number, string or atom";
    decode_error $txt, $at, $bad_json unless defined $txt;
    my $len = length $txt;
    ($at, $ch) = next_chr $txt, $len, $at;
    ($at, $ch) = white $txt, $len, $at, $ch; # remove head white space
    decode_error $txt, $at, $bad_json unless $ch;
    ($at, $ch, $result) = value $txt, $len, $at, $ch;
#($at, $ch) = next_chr $txt, $len, $at;
    die 'Something went terribly wrong, we read past EOF' if $len < $at;
    ($at, $ch) = white $txt, $len, $at, $ch; # remove tail white space
    decode_error $txt, $at, "garbage after JSON object" if $ch;
    $result;
}

print Dumper decode_json $test;

1;

__END__

=pod

=head1 NAME

JSON - Based on JSON::PP by Makamaka Hannyaharamitu

=head1 SYNOPSIS

 use JSON;

 # exported functions, they croak on error

 decode_json

=cut

