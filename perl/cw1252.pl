#!/usr/bin/env perl

# Cleans a mixture of ascii, microsoft and iso_8859-1
# encoding into pure ascii encoding.
# Tries to not mangle utf-8 characters.

while (<>) {
 s/([^\x80-\xff])\x80/$1E=/;       # windows-1251 euro sign            => E=
 s/([^\x80-\xff])\x82/$1'/g;       # windows-1251 single quote         => '
 s/([^\x80-\xff])\x83/$1f/g;       # windows-1251 function sign        => f
 s/([^\x80-\xff])\x84/$1"/g;       # windows-1251 double quote         => "
 s/([^\x80-\xff])\x85/$1.../g;     # windows-1251 ellipsis             => ...
 s/([^\x80-\xff])\x86/$1+/g;       # windows-1251 dagger               => +
 s/([^\x80-\xff])\x87/$1++/g;      # windows-1251 double dagger        => ++
 s/([^\x80-\xff])\x88/$1^/g;       # windows-1251 carret               => ^
 s{([^\x80-\xff])\x89}{$1\1000}g;  # windows-1251 permille             => /1000
 s/([^\x80-\xff])\x8a/$1S/g;       # windows-1251 capital strong s     => S
 s/([^\x80-\xff])\x8b/$1</g;       # windows-1251 less than            => <
 s/([^\x80-\xff])\x8c/$1CE/g;      # windows-1251 capital letter ce    => CE
 s/([^\x80-\xff])\x8e/$1Z/g;       # windows-1251 capital strong z     => Z
 s/([^\x80-\xff])\x91/$1'/g;       # windows-1251 backquote            => '
 s/([^\x80-\xff])\x92/$1'/g;       # windows-1251 acute accent         => '
 s/([^\x80-\xff])\x93/$1"/g;       # windows-1251 left "smart" quote   => "
 s/([^\x80-\xff])\x94/$1"/g;       # windows-1251 right "smart" quote  => "
 s/([^\x80-\xff])\x95/$1./g;       # windows-1251 middle dot           => .
 s/([^\x80-\xff])\x96/$1-/g;       # windows-1251 dash                 => -
 s/([^\x80-\xff])\x97/$1-/g;       # windows-1251 dash                 => -
 s/([^\x80-\xff])\x98/$1~/g;       # windows-1251 tilde                => ~
 s/([^\x80-\xff])\x99/$1(TM)/g;    # windows-1251 trademark sign       => (TM)
 s/([^\x80-\xff])\x9a/$1s/g;       # windows-1251 small strong s       => s
 s/([^\x80-\xff])\x9b/$1>/g;       # windows-1251 more than            => >
 s/([^\x80-\xff])\x9c/$1ce/g;      # windows-1251 small letter ce      => ce
 s/([^\x80-\xff])\x9e/$1z/g;       # windows-1251 small strong z       => z
 s/([^\x80-\xff])\x9f/$1Y/g;       # windows-1251 capital umlaut y     => Y

 s/([^\x80-\xff])\xa0/$1 /g;      # iso_8859-1 non breakable space     => 0x20
 s/([^\x80-\xff])\xa1/$1!/g;      # iso_8859-1 inverted exclamation    => !
 s/([^\x80-\xff])\xa2/$1c/g;      # iso_8859-1 cent sign               => c
 s/([^\x80-\xff])\xa3/$1P/g;      # iso_8859-1 pound sign              => P
 s/([^\x80-\xff])\xa4/$1C/g;      # iso_8859-1 currency sign           => C
 s/([^\x80-\xff])\xa5/$1Y/g;      # iso_8859-1 yen sign                => Y
 s/([^\x80-\xff])\xa6/$1|/g;      # iso_8859-1 broken bar              => |
 s/([^\x80-\xff])\xa7/$1S/g;      # iso_8859-1 section sign            => S
 s/([^\x80-\xff])\xa8/$1"/g;      # iso_8859-1 diaeresis               => "
 s/([^\x80-\xff])\xa9/$1(C)/g;    # iso_8859-1 copyright sign          => (C)
 s/([^\x80-\xff])\xaa/$1^a/g;     # iso_8859-1 fenimine ordinal        => ^a
 s/([^\x80-\xff])\xab/$1<</g;     # iso_8859-1 left double angle       => <<
 s/([^\x80-\xff])\xac/$1-/g;      # iso_8859-1 not sign                => -
 s/([^\x80-\xff])\xad/$1-/g;      # iso_8859-1 soft hyphen             => -
 s/([^\x80-\xff])\xae/$1(R)/g;    # iso_8859-1 registered sign         => (R)
 s/([^\x80-\xff])\xaf/$1^-/g;     # iso_8859-1 macron                  => ^-
 s/([^\x80-\xff])\xb0/$1^o/g;     # iso_8859-1 degree sign             => ^o
 s/([^\x80-\xff])\xb1/$1+-/g;     # iso_8859-1 plus minus              => +-
 s/([^\x80-\xff])\xb2/$1^2/g;     # iso_8859-1 superscript two         => ^2
 s/([^\x80-\xff])\xb3/$1^3/g;     # iso_8859-1 superscript three       => ^3
 s/([^\x80-\xff])\xb4/$1'/g;      # iso_8859-1 acute accent            => '
 s/([^\x80-\xff])\xb5/$1m/g;      # iso_8859-1 micro sign              => m
 s/([^\x80-\xff])\xb6/$1|P/g;     # iso_8859-1 pilcrow                 => |P
 s/([^\x80-\xff])\xb7/$1*/g;      # iso_8859-1 middle dot              => *
 s/([^\x80-\xff])\xb8/$1,/g;      # iso_8859-1 cedilla                 => ,
 s/([^\x80-\xff])\xb9/$1^1/g;     # iso_8859-1 superscript one         => ^1
 s/([^\x80-\xff])\xba/$1^o/g;     # iso_8859-1 masculine ordinal       => ^o
 s/([^\x80-\xff])\xbb/$1>>/g;     # iso_8859-1 right double angle      => >>
 s!([^\x80-\xff])\xbc!${1}1/4!g;  # iso_8859-1 one quarter             => 1/4
 s!([^\x80-\xff])\xbd!${1}1/2!g;  # iso_8859-1 one half                => 1/2
 s!([^\x80-\xff])\xbe!${1}3/4!g;  # iso_8859-1 three quarters          => 3/4
 s/([^\x80-\xff])\xbf/$1?/g;      # iso_8859-1 inverted question mark  => ?

 s/([^\x80-\xff])\xc6/$1AE/g;   # iso_8859-1 capital letter ae    => AE
 s/([^\x80-\xff])\xd0/$1=D/g;   # iso_8859-1 capital letter eth   => =D
 s/([^\x80-\xff])\xd7/$1*/g;    # iso_8859-1 multiplication sign  => *
 s/([^\x80-\xff])\xd8/${1}0/g;  # iso_8859-1 capital stroke       => 0
 s/([^\x80-\xff])\xdf/$1B/g;    # iso_8859-1 sharp s              => B
 s/([^\x80-\xff])\xe6/$1ae/g;   # iso_8859-1 letter ae            => ae
 s/([^\x80-\xff])\xf0/$1=d/g;   # iso_8859-1 letter eth           => =d
 s{([^\x80-\xff])\xf7}{$1/}g;   # iso_8859-1 division sign        => /
 s/([^\x80-\xff])\xf8/${1}0/g;  # iso_8859-1 stroke               => 0

    print;
}

