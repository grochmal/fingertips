#!/bin/env perl
###############################################################################
# development_eod.pl
#
# Integrates changes made to mainline into development branches
# Copyright (c) FIS
# Author Michal Grochmal
###############################################################################

#################################### Packages #################################
use strict;
use warnings;
use Getopt::Std;

#################################### Defines ##################################
# This is quite useful
use constant True => 1;
use constant False => 0;

#################################### Globals ##################################
# Structure hash
my %mainline_hash = ();


###############################################################################
# main
#
# Organize subroutines (intuitive)
###############################################################################
my ($client, $check, $debug, $file_name, $print, $silent, $verbose) = &init;
&parse_branch_file;
&print_branch_structure if($debug or $print);
&check_client if($check);

# After all the stuctures are built do the integration
foreach my $main (keys %mainline_hash) {
    foreach my $branch (@{$mainline_hash{$main}}) {
        &integrate_branch($main, $branch);
    }
}

###############################################################################
# init
#
# Check the command line arguments, check the environment and prints help
###############################################################################
sub init {
    our($opt_c, $opt_d, $opt_f, $opt_h, $opt_k, $opt_p, $opt_s, $opt_v);
    my $usage = "Usage:\n" .
        "\tdevelopment_eod.pl [-h] -f <branches file> " .
            "-c <p4 client to use> [-s]\n" .
            "\t\t[-v] [-d] [-k]\n\n" .
        "\t-f branches file, for an example of the file use -h option\n" .
        "\t-h prints the help\n" .
        "\t-k checks the client before trying to integrate, it's\n" .
        "\t\ta separate option because takes some time to do it\n" .
        "\t-p parses the branch file and prints an integration table\n" .
        "\t-s silent mode, does not promt for confirmation (always \"y\")\n" .
        "\t-v turn on verbose mode\n" .
        "\t-d sets the debug on (implies -v)\n\n";
    my $extra_help =
        "This script will read a branch file and then run the integration\n" .
        "through the client specified with -c option (if the client does\n" .
        "not see all branches in the branch file the script fails).\n\n" .
        "Example of a branch file(<TAB> means horizontal tab,\n" .
        "<NL> means new line, lines starting with # are comments):\n\n" .
        "##############################<NL>\n" .
        "# This is the mainline for crdbase<NL>\n" .
        "//depot/cortex/v2/v2.3/base/crdbase/...    <NL>\n" .
        "<TAB>//depot/cortex/project/cortex_v3.2.1/base/crdbase/...<NL>\n" .
        "<TAB>//depot/cortex/project/cortex_v3.2.2/crdbase/...   <NL>\n" .
        "<NL>\n" .
        "# Mainline for iso93 inteface<NL>\n" .
        "//depot/cortex/v2/v2.3/base/iso93/...<NL>\n" .
        "    //depot/cortex/project/cortex_v3.2.1/base/iso93/...<NL>\n" .
        "# Notice that we didn't used a tab in the line above<NL>\n" .
        "# but spaces, this is allowed and will be <NL>". 
        "# undestood correctly<NL>\n" .
        "<NL>\n" .
        "##############################<NL>\n\n" .
        "Limitations:\n" .
        "\tIt only undestands branches with /... at the end, syntax using\n" .
        "\t/....c, /*.c, .../*, filemname... or containig ... two times\n" .
        "\twill be ignored\n\n" .
        "\tThe same syntax limitation is bound to client views when\n" .
        "\tchecking the client having a branch //depot/cortex/... in\n" .
        "\tthe branch file and //depot/... in the client view will be\n" .
        "\tunderstood, but //depot... or //depot/.../... will be treated\n" .
        "\tas an error\n\n" .
        "\tYou cannot integrate from a mainline to a branch and then use\n" .
        "\tthis branch as a mainline in a subsequent integration,\n" .
        "\tthis is to ensure that no overlaping integrations can happen.\n" .
        "\tActually there's nothing that constrains you from doing it\n" .
        "\tbut trying to integrate into a branch and then from this\n" .
        "\tbranch to another in one go may leave some changes not\n" .
        "\tbeing integrated\n\n" .
        "\tA workaround about it is to wrap this script into a short\n" .
        "\tshell script that uses different branch files each time:\n\n" .
        "##############################\n" .
        "#!/bin/sh\n" .
        "development_eod.pl -f mainline_to_dev.vctl -c myclient -s\n" .
        "development_eod.pl -f dev1_to_dev2.vctl -c devel_client -s\n" .
        "development_eod.pl -f dev2_to_dev3.vctl -c devel_client -s\n" .
        "##############################\n\n" .
        "Note that we use a different client for the development branches\n" .
        "this is not necessary, but supported and a valid way of doing\n" .
        "the integration\n\n";


    $opt_c = "";
    $opt_d = False;
    $opt_f = "";
    $opt_h = False;
    $opt_p = False;
    $opt_s = False;
    $opt_v = False;

    # Test the argument number or if the user wants help
    die $usage if(-1 eq $#ARGV); # No arguments passed
    die $usage if(!getopts("c:df:hkpsv")); # Unknown options
    die $usage . $extra_help if(1 eq $opt_h); # Extra help
    die $usage if("" eq $opt_f); # -f not set
    die $usage if("" eq $opt_c); # -c not set

    # Setup globals from command line
    $client = $opt_c;
    $debug = $opt_d;
    $verbose = $opt_d;
    $file_name = $opt_f;
    $print = $opt_p;
    $silent = $opt_h;
    $check = $opt_k;

    # Try setting verbose  only if not already set
    $verbose = $opt_v if(not $verbose);

    # debug
    print "Command line parsed as:\n" .
        "Client to be used: $client\n" .
        "Template file: $file_name\n" .
        "Print table: " . ($print ? "True" : "False") . "\n" .
        "Silent mode: " . ($silent ? "True" : "False") . "\n" .
        "Verbose: " . ($verbose ? "True" : "False") . "\n" .
        "Debug: " . ($debug ? "True" : "False") . "\n"
        if($debug);

    $opt_c, $opt_k, $opt_d, $opt_f, $opt_p, $opt_h, $opt_v;
}

###############################################################################
# parse_branch_file
#
# Retrieves the branches from the branch file
###############################################################################
sub parse_branch_file {
    my $line;
    my $line_num = 0;
    my $mainline;

    # These tow are only for a better control and debug / verbose
    my $adding_branches = False;
    my $main_empty = False;


    open(BRANCHFILE, "<$file_name") or die "File $file_name does not exist";

    # verbose
    print "****** Processing branch file [$file_name] ******\n" if ($verbose);

    while(<BRANCHFILE>) {
        $line_num++;

        # verbose
        print "****** Processing line [$line_num] ******\n" if($debug);

        # debug
        print $_ if($debug);

        if($_ =~ m/^\s*#/)
        {
            print "A comment\n" if($debug);
            next;
        }

        if($_ =~ m/^\s*$/)
        {
            print "An empty line\n" if($debug);
            next;
        }

        if($_ =~ m/^\/\//)
        {
            chomp($_);

            # Syntax not understood
            if($_ !~ m/\/\.\.\.$/ or $_ =~ m/\.\.\.\/\.\.\./)
            {
                # verbose
                print "Warning: Bad syntax in mainline [$_], ignoring\n"
                    if($verbose);
                next;
            }

            # verbose (Warning: nothing was added to the last mainline)
            print "Warning: Nothing was added to mainline $mainline\n"
                if($verbose and $main_empty);

            # Set mainline only if all ignoring conditions are not met
            $mainline = $_;

            # Remove trailing space in any case
            $mainline =~ s/\s+$//;

            # verbose
            print "Found mainline [$mainline]\n" if ($verbose);

            # Now we can start adding branches
            $adding_branches = True;
            $main_empty = True;
            next;
        }

        if($_ =~ m/^\s+\/\//)
        {
            $line = $_;
            chomp($line);

            # Remove trailing space in any case
            $line =~ s/\s+$//;

            # debug
            print "Got a line that looks like a branch\n" if($debug);

            # Syntax not understood
            if($line !~ m/\/\.\.\.$/ or $line =~ m/\.\.\.\/\.\.\./)
            {
                # verbose
                print "Warning: Bad syntax in branch [$line], ignoring\n"
                    if($verbose);
                next;
            }

            if($adding_branches)
            {
                # Remove indentation before adding to structure
                $line =~ s/^\s*//;

                push(@{$mainline_hash{$mainline}}, $line);
                $main_empty = False;

                # verbose
                print "\tAdding branch [$line] to mainline [$mainline]\n"
                    if ($verbose);
            }
            else
            {
                # We are trying to add a branch but no mainline was specified
                print "Warning: No mainline was specified for branch $line " .
                    "ignoring the branch\n" if($verbose);
            }

            next
        }

        # If we got here the line matched nothing, warn the user
        chomp($_);
        print "Warning: The branch spec [$_] is invalid, ignoring\n"
            if($verbose);
    }

    close(BRANCHFILE);
}

###############################################################################
# print_branch_structure
#
# Debug subroutine to print if how we parsed the branch file
###############################################################################
sub print_branch_structure
{
    print "****** Structure in the branch file looks as follows: ******\n";

    foreach my $main (keys %mainline_hash)
    {
        print "From $main integrate to:\n";
        foreach my $branch (@{$mainline_hash{$main}})
        {
            print "\t\\-> $branch\n";
        }

        print "******\n";
    }

    die "Remove -p flag to start the integration" if($print);
}

###############################################################################
# check_client
#
# Very simple check to verify if the branches from the branch file are
# visible to the client
###############################################################################
sub check_client
{
    my @client_view = ();
    my $linenum = 0;


    # verbose
    print "****** Check client $client ******\n" if($verbose);

    open(CLIENT, "p4cl client -o $client |")
        or die "Client $client doesn't exist";

    while(<CLIENT>)
    {
        # Ignore not path lines
        # Path may start as //depot or "//depot for each need a different logic
        if($_ =~ m/^\t"\/\//)
        {
            my $line = $_;


            chomp($line);

            # Syntax to be ignored understood
            if($line !~ m/\/\.\.\.$/ or $line =~ m/\.\.\.\/\.\.\./)
            {
                # debug
                print "Syntax will not be used for checks [$line]\n"
                    if($debug);
                next;
            }

            # debug
            print "****** Processing line [$linenum] [$line] ******\n"
                if($debug);

            # Get the view in depot path
            $line =~ s/^\s*//;
            $line =~ m/^"([^"]*)"/;
            $line = $1;

            #debug
            print "Found view to [$line]\n" if ($debug);

            push(@client_view, $line);
        }
        elsif($_ =~ m/^\t\/\//)
        {
            my $line = $_;


            chomp($line);

            # Syntax to be ignored understood
            if($line !~ m/\/\.\.\.$/ or $line =~ m/\.\.\.\/\.\.\./)
            {
                # debug
                print "Syntax will not be used for checks [$line]\n"
                    if($debug);
                next;
            }

            # debug
            print "****** Processing line [$linenum] [$line] ******\n"
                if($debug);

            # Get the view in depot path
            $line =~ s/^\s*//;
            $line =~ m/^(\/\/[^\s]*)\s\/\//;
            $line = $1;

            #debug
            print "Found view to [$line]\n" if ($debug);

            push(@client_view, $line);
        }

        $linenum++;
    }

    close(CLIENT);

    print "Full client view\n" . join("\n", @client_view) . "\n" if($debug);

    # Check each mainline and each branch linked to it
    foreach my $main (keys %mainline_hash)
    {
        my $match = False;


        # verbose
        print "Trying to match mainline [$main] in client\n" if($verbose);

        # Check mainline
        $match = False;
        foreach my $view (@client_view)
        {
            my $tmp_main;


            # Only lines with /... at the end gets here
            $tmp_main = $main;
            $tmp_main =~ s/\/\.\.\.$//;
            $view =~ s/\/\.\.\.$//;


            # While there's still something in the mainline string
            while($tmp_main =~ m/\/\/depot\//)
            {
                # debug
                print "Comparing mainline [$tmp_main] and view [$view]\n"
                    if($debug);

                if($tmp_main eq $view)
                {
                    # debug
                    print "\tIt's a match!\n" if($verbose);

                    $match = True;
                    last;
                }

                # Matched mainline can be longer than the view
                $tmp_main =~ s/\/[^\/]*$//;
            }
        }

        die "Could not find a match in the client view for " .
            "mainline [$main]\n" if(not $match);

        # Check branches
        $match = False;
        foreach my $branch (@{$mainline_hash{$main}})
        {
            my $tmp_branch = $branch;


            # verbose
            print "Trying to match branch [$branch] in client\n" if($verbose);

            $tmp_branch =~ s/\/\/\.\.\.//;
            $match = False;
            foreach my $view (@client_view)
            {
                my $tmp_branch;


                # Only lines with /... at the end gets here
                $tmp_branch = $branch;
                $tmp_branch =~ s/\/\/\.\.\.$//;
                $view =~ s/\/\.\.\.$//;

                # While there's still something in the branch string
                while($tmp_branch =~ m/\/\/depot\//)
                {
                    # debug
                    print "Comparing branch [$tmp_branch] and view [$view]\n"
                        if($debug);

                    if($tmp_branch eq $view)
                    {
                        # debug
                        print "\tIt's a match!\n" if($verbose);

                        $match = True;
                        last;
                    }

                    # Matched branch can be longer than the view
                    $tmp_branch =~ s/\/[^\/]*$//;
                }
            }

            die "Could not find a match in the client view for " .
                "branch [$branch]\n" if(not $match);
        }
    }

    # If we got here it's because everything matched!
    print "****** The p4 client matches the specifications form the " .
        "branch file\n" if($verbose);
}

###############################################################################
# integrate_branch
#
# Performs a test integration and if there's something that needs integrating
# it asks for confirmation
#
# Also available a silent mode where all confirmations are skipped (interpreted
# as 'y'). Note that in silent mode no files can be deleted and conflicting
# files needs to be resolved by hand anyway in the silent mode.
###############################################################################
sub integrate_branch
{
    my($main, $branch) = @_;
    my $flags = '-n';
    my $done = False;
    my $show = False;
    my %options =
        ('flags' => 'flags',
        'Flags' => 'flags',
        'f' => 'flags',
        'F' => 'flags',
        'integrate' => 'integrate',
        'Integrate' => 'integrate',
        'i' => 'integrate',
        'I' => 'integrate',
        'next' => 'next',
        'Next' => 'next',
        'n' => 'next',
        'N' => 'next',
        'show' => 'show',
        'Show' => 'show',
        's' => 'show',
        'S' => 'show');


    # There's something to do within this branch
    while(not $done)
    {
        # Default
        my $function = 'i';
        my $cmd;


        # If we done something when on the last loop set done
        # this makes us loop only two times for each branch
        # (unless we set -n in the flags
        $done = True if(not '-n' eq $flags);

        # debug
        print "Using flags [$flags]\n" if($debug);

        # Remember to add 2>&1 on all p4cl commands
        # it prints all to stderr, hate u p4cl :(
        $cmd = "p4cl -c $client integrate $flags $main $branch 2>&1";

        # verbose
        print "************************************\n" if ($verbose);
        print "$cmd\n" if($verbose or $show);
        open(INTEGRATION, "$cmd |");

        while(<INTEGRATION>)
        {
            print $_ if($verbose or $show);
            chomp($_);

            # If we have nothing to do for this branch set done straight away
            if(not $done and
                $_ =~ m/-\sall\srevision\(s\)\salready\sintegrated\./)
            {
                print "Nothing to do for this branch\n" if($verbose);
                $done = True;
            }
        }

        close(INTEGRATION);

        # Not silent
        if(not $done and not $silent)
        {
            my $decision = "nothing";


            # Loop unti gets the correct input
            while(not exists $options{$decision} and not "" eq $decision)
            {
                print "Needs to integrate into $branch\n" .
                    "Do it?\n" .
                    "[i] integrate -> will perform the integration\n" .
                    "[n] next -> go to the next branch, ignore current\n" .
                    "[f] flags -> specify flags and do the integration\n" .
                    "[s] show -> show the integrations (same as flags -n)\n" .
                    "Use option f only if you know what are you doing\n" .
                    "i, n, f, s (default is i): ";

                $decision = <>;
                chomp($decision);

                # debug
                print "Option selected is [$decision]\n" if($debug);
            }

            # Correct the default input, this prevents endless loop
            $decision = 'i' if("" eq $decision);

            # Get the option in words understood by the script
            $function = $options{$decision};

            print "For the script it means [$function]\n" if($debug);
        }

        if(not $done)
        {
            # debug
            print "Deciding which flag to use\n" if($debug);

            if('flags' eq $function)
            {
                $show = True;
                $flags = "";
                print "Please input flags to be used in the p4 integrate: ";
                while("" eq $flags)
                {
                    $flags = <>;
                    chomp($flags);

                    # debug
                    print "Flags selected are [$flags]\n" if($debug);
                }
            }
            elsif('integrate' eq $function)
            {
                $flags = "";
                $show = True;
            }
            elsif('next' eq $function)
            {
                $done = True;
            }
            elsif('show' eq $function)
            {
                $flags = '-n';
                $show = True;
            }
            else
            {
                print "Unrecognized function!!!";
            }
        }
    }
}

