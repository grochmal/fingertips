#!/usr/bin/env perl

# parsing for numbers

use strict;
use warnings;

open my $nums, "<input" or die "input: no such file: $!";
my $comps = [];
while (<$nums>) {
  no warnings;
  push @$comps, int $_;
}
close $nums;
@$comps = grep { $_ > 0 } @$comps;
my $comb = [];
foreach my $f (@$comps) {
  push @$comb, "$f ($f)";
  foreach my $s (@$comps) {
    my $tmp = $f + $s;
    push @$comb, "$tmp ($f+$s)";
    foreach my $t (@$comps) {
      my $tmp = $f + $s + $t;
      push @$comb, "$tmp ($f+$s+$t)";
    }
  }
}
no warnings;
foreach (sort { int $a <=> int $b } @$comb) {
  print "$_\n";
}
use warnings;

