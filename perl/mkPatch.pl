#!/bin/env perl
######
# mkPatch.pl
#
# Creates a patch file from the a perforce repository
# Copyright (c) under GPL 3
# Author Michal Grochmal
######

use strict;
use warnings;

# Globals
# Holds the job name, the changelists and the diff program between subroutines
my $job_name;
my $diff_prog;
my @changelists = ();

######
# main
#
# Organize subroutines (intuitive)
######

&init;
&check_place;
&find_changelists;

foreach (@changelists)
{
    &find_files($_);
}

######
# init
#
# Check the command line arguments, check the environmet and prints help
######

sub init
{
    my $argNum = $#ARGV + 1; # Number of arguments
    my $usage = "\nUsage:\n\t\tmkPatch [-h | --help] <job name>\n\n" .
        "Create a patch file from all files involving the perforce job.\n" .
        "For edited files this script depends on a GNU compatible diff,\n" .
        "if you are unsure about your diff program you can download a GNU\n" .
        "compatible diff through the open source binutils package.\n" .
        "As perforce's diff (p4 diff ...) isn't GNU compatible we need\n" .
        "to use a different diff program.\n" .
        "This script suposes the name of the GNU compatible diff as\n" .
        "simply \"diff\" (it's true on most linux systems, on the other\n" .
        "hand on most comercial UNIX system it's not). If you have a\n" .
        "different name for your diff please setup a \"PATCHDIFF\"\n" .
        "environment variable to the diff program name to be used by this\n" .
        "script and, consequently, by the perforce diff's\n\n";

    # Test the argument number or if the user wants help
    if($argNum != 1)
    {
        die $usage;
    }
    elsif($ARGV[0] eq "-h" || $ARGV[0] eq "--help")
    {
        die $usage;
    }
    else
    {
        $job_name = $ARGV[0];
    }

    # Test is such perforce job really exist
    if(0 != system("p4 jobs | grep $job_name > /dev/null 2>&1"))
    {
        die "No such p4 job!\n";
    }

    # Test if we are using some different diff from the default
    if(exists $ENV{'PATCHDIFF'})
    {
        $diff_prog = $ENV{'PATCHDIFF'};
    }
    else
    {
        $diff_prog = "diff";
    }
}

######
# check_place
#
# Check if we are inside a p4 client, otherwise we cannot continue
######

sub check_place
{
    open(CLIENT, "p4 info |");
    my @client = <CLIENT>;
    close(CLIENT);

    chomp($client[0]);
    if("You are not in a valid client workspace" eq $client[0])
    {
        die "You are not in a valid client workspace\n";
    }
}

######
# find_changelists
#
# Find the changelists related to this job and store in @changelists
######

sub find_changelists
{
    open(JOB, "p4 fixes -j $job_name |");
    while(<JOB>)
    {
        my $line = $_;
        chomp($line);

        $line =~ m/change\s[0-9]*\s/;
        $line = $&;

        $line =~ m/[0-9]+/;
        $line = $&;

        ######
        # For perl 5.10 and above
        # $line =~ m/change\s[0-9]*\s/p;
        # $line = ${^MATCH};

        # $line =~ m/[0-9]+/;
        # $line = ${^MATCH};
        ######

        &add_to_array($line);
    }

    close(JOB);
}

######
# add_to_array
#
# Add only distinct values to array
######

sub add_to_array
{
    my($change) = @_;
    my $is_in_array = 'NO';

    foreach my $change_in_array (@changelists)
    {
        if($change_in_array eq $change)
        {
            $is_in_array = 'YES';
        }
    }

    if($is_in_array ne 'YES')
    {
        push(@changelists, $change);
    }
}

######
# find_files
#
# Lookup through the changelists for opened files and ditinguish them into
# added files, edited files and deleted files
######

sub find_files
{
    my($changelist) = @_;

    open(CHANGE, "p4 opened -c $changelist 2>&1 |");
    while(<CHANGE>)
    {
        my $line = $_;
        chomp($line);

        if($line =~ m/#[0-9]*\s-\sedit\s/) # Edit change
        {
            $line = $`;
            &edited_file($line);
        }
        elsif($line =~ m/#[0-9]*\s-\sadd\s/) # Add change
        {
            $line = $`;
            &added_file($line);
        }
        elsif($line =~ m/#[0-9]*\s-\sdelete\s/) # Delete change
        {
            $line = $`;
            &deleted_file($line);
        }
        elsif($line =~ m/#[0-9]*\s-\sintegrate\s/) # Integrate change
        {
            $line = $`;
            &edited_file($line);
        }
        elsif($line =~ m/#[0-9]*\s-\sbranch\s/) # Branch change
        {
            $line = $`;
            &added_file($line);
        }
    }

    close(CHANGE);
}

######
# edited_file
#
# Uses p4 diff to create a patch from a file
######

sub edited_file
{
    my($depot_name) = @_;
    my $file_change = "#1";

    $ENV{'P4DIFF'} = $diff_prog;

    open(DIFF, "p4 diff -dU10000 $depot_name |");
    while(<DIFF>)
    {
        my $line = $_;
        chomp($line);

        if($line =~ m/^==== .*(#[0-9]+) - .*/)
        {
            $file_change = $1;
        }
        if($line =~ m/^--- /)
        {
            $line =~ s/(^--- ).*(\t.*)/${1}${depot_name}${file_change}${2}/;
        }

        print $line . "\n";
    }

    close(DIFF);
}

######
# added_file
#
# Find the added file in our eviroment and print it patch like
######

sub added_file
{
    my($depot_name) = @_;
    my $count = 0;

    open(ADD, "p4 where $depot_name |");
    my @place = <ADD>;
    close(ADD);

    chomp($place[0]);
    $place[0] =~ s/^[^\s]*\s[^\s]*\s//;

    print "==== " . $depot_name . " - " . $place[0] . " ====\n";
    print "--- " . $depot_name . "\n";
    print "+++ " . $place[0] . "\n";

    open(ADDFILE, "<", $place[0]);
    $count++ while(<ADDFILE>);
    close(ADDFILE);

    print "@@ -1," . $count . " +1," . $count . " @@\n";

    open(ADDFILE, "<", $place[0]);
    while(<ADDFILE>)
    {
        my $line = "+" . $_;
        print $line;
    }

    close(ADDFILE);
}

######
# deleted_file
#
# Print the file from the depot's last version
######

sub deleted_file
{
    my($depot_name) = @_;
    my $count = 0;

    open(DELETE, "p4 where $depot_name |");
    my @place = <DELETE>;
    close(DELETE);

    chomp($place[0]);
    $place[0] =~ s/^[^\s]*\s[^\s]*\s//;

    print "==== " . $depot_name . " - " . $place[0] . " ====\n";
    print "--- " . $depot_name . "\n";
    print "+++ " . $place[0] . "\n";

    open(DELETEFILE, "p4 print -q $depot_name |");
    $count++ while(<DELETEFILE>);
    close(DELETEFILE);

    print "@@ -1," . $count . " +1," . $count . " @@\n";

    open(DELETEFILE, "p4 print -q $depot_name |");
    while(<DELETEFILE>)
    {
        my $line = "-" . $_;
        print $line;
    }

    close(DELETEFILE);
}

