#!/usr/bin/env perl

use strict;
use warnings;

my $keytab;
$keytab .= unpack "H*", $_ while (<>);
my @keys = $keytab =~ m/([0-9abcdef]{320})/g;
my ($mac, $seed) = $keytab =~ m/([0-9abcdef]{16})([0-9abcdef]{32})$/;
print "$_\n\n" foreach (@keys);
print "$mac\n"; print "$seed\n"

