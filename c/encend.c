/* encryption problems with endianess */
#include <stdio.h>
#include <string.h>

/*{
char *bufhex = calloc(size*2, 1);
char hexnum[2+1];
for (i = 0; i < size; i++) {
    sprintf(hexnum, "%02x", *(buf+i));
    strncpy(bufhex+i*2, hexnum, 2);
}
DBG_PRINTF((dbg_always, "data in %s", bufhex));
free(bufhex);
}*/

static void
enc_long2key(unsigned int s[2], char *d)
{
	sprintf(d, "%08X", s[0]);
	sprintf(d+8, "%08X", s[1]);
}

static void
enc_key2long(char *key, unsigned int *k)
{
	int i;
	int len = (int)strlen(key) / 16;

	for(i = 0; i < len; i++) {
		sscanf(key+(i*16), "%8X%8X",
		       &k[(i*2)], &k[(i*2)+1]);
	}
}

static void
end(void)
{
	unsigned int num = 0;
	unsigned int i;
	size_t size = sizeof(num);
	char *p = (char *)&num;
	p[0] = 8;       p[1] = 8;
	p[0] >>= 1;     p[1] >>= 1;
	printf("[%x]\n", num);
	p[0] = 8;       p[1] = 8;
	p[0] <<= 1;     p[1] <<= 1;
	printf("[%x]\n", num);
	p[0] = 8;       p[1] = 8;
	p[0] >>= 4;     p[1] >>= 4;
	printf("[%x]\n", num);
	p[0] = 8;       p[1] = 8;
	num >>= 4;
	printf("[%x]\n", num);
	num = 0;
	p[0] = 8;       p[1] = 8;
	num <<= 5;
	printf("[%x]\n", num);
	num = 0;
	p[0] = 8;       p[1] = 8;
	/*p[0] >>= 4;*/ p[1] >>= 4;
	printf("[%x]\n", num);
	p[0] = 8;       p[1] = 8;
	p[0] <<= 5;     p[1] <<= 5;
	printf("[%x]\n", num);
	p[0] = 8;       p[1] = 8;
	p[0] <<= 5;     /*p[1] <<= 5;*/
	printf("[%x]\n", num);
	for (i = 0; i < size; i++)
		p[i] = 8;
	printf("[%x]\n", num);
}

int
main(int argc, char **argv)
{
	char key[] = "0123456789ABCDEF";
	char new_key[16+1];
	unsigned int key_l[2] = {0,0};
	end();
	enc_key2long(key, key_l);
	enc_long2key(key_l, new_key);
	printf("[%s][%s]\n", key, new_key);
	printf("[%u][%u]\n", key_l[0], key_l[1]);
	return 0;
}

