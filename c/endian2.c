
#include <stdio.h>

static void
build_bytes(void *data, size_t data_size)
{
	char *p;
	size_t i;

	for (p = data, i = 1; i <= data_size; i++, p++)
		*p = (char)(i & 0xff);
}

static void
print_byte_order(void *data, size_t data_size)
{
	char *p;
	size_t i;

	printf("Data representation: [ ");
	for (p = data, i = 1; i <= data_size; i++, p++)
		printf("%02x ", (*p) & 0xff);
	printf("] ");
}

static void
print_shift(void *data, size_t data_size, char *op)
{
	char *p;
	size_t i;

	print_byte_order(data, data_size);
	printf("%s\n", op);
}

int
main (int argc, char **argv)
{
	size_t size_t_size = sizeof(size_t);
	size_t char_size   = sizeof(unsigned char);
	size_t int_size    = sizeof(unsigned int);
	size_t short_size  = sizeof(unsigned short int);
	size_t long_size   = sizeof(unsigned long int);
	size_t float_size  = sizeof(float);
	size_t double_size = sizeof(double);
	size_t cptr_size   = sizeof(char *);
	size_t vptr_size   = sizeof(void *);
	unsigned char      end_char      = ~0;
	unsigned char      tmp_char      = ~0;
	unsigned int       end_int       = ~0;
	unsigned int       tmp_int       = ~0;
	signed int         end_minus_int = -5;
	signed int         tmp_minus_int = -5;
	signed int         end_plus_int  =  5;
	signed int         tmp_plus_int  =  5;
	unsigned short int end_short     = ~0;
	unsigned short int tmp_short     = ~0;
	unsigned long int  end_long      = ~0;
	unsigned long int  tmp_long      = ~0;
	float              end_float     = ~0;
	float              tmp_float     = ~0;
	double             end_double    = ~0;
	double             tmp_double    = ~0;

	build_bytes(&end_char,   char_size);
	build_bytes(&end_int,    int_size);
	build_bytes(&end_short,  short_size);
	build_bytes(&end_long,   long_size);
	build_bytes(&end_float,  float_size);
	build_bytes(&end_double, double_size);

	printf("sizeof size_t             = [%i]\n", size_t_size);
	printf("sizeof unsigned char      = [%i]\n", char_size);
	printf("sizeof unsigned int       = [%i]\n", int_size);
	printf("sizeof unsigned short int = [%i]\n", short_size);
	printf("sizeof unsigned long int  = [%i]\n", long_size);
	printf("sizeof float              = [%i]\n", float_size);
	printf("sizeof double             = [%i]\n", double_size);
	printf("sizeof char *             = [%i]\n", cptr_size);
	printf("sizeof void *             = [%i]\n", vptr_size);

	print_byte_order(&end_char, char_size);
	printf("unsigned char [%hi] [%lx]\n",       end_char, end_char);
	print_byte_order(&end_int, int_size);
	printf("unsigned int [%i] [%x]\n",         end_int, end_int);
	print_byte_order(&end_short, short_size);
	printf("unsigned short int [%hi] [%lx]\n", end_short, end_short);
	print_byte_order(&end_long, long_size);
	printf("unsigned long int [%li] [%lx]\n",  end_long, end_long);
	print_byte_order(&end_float, float_size);
	printf("float [%f] [%lx]\n",               end_float, end_float);
	print_byte_order(&end_double, double_size);
	printf("double [%lf] [%lx]\n",             end_double, end_double);

	printf("According to ANSI C x << 1 must be equal to x * 2,\n"
	       "therefore the vacated bit must be filled with 0.\n");
	tmp_char   = end_char  << 1;
	tmp_int    = end_int   << 1;
	tmp_short  = end_short << 1;
	tmp_long   = end_long  << 1;
	print_shift(&tmp_char,  char_size,  "unsigned char << 1"     );
	print_shift(&tmp_int,   int_size,   "unsigned int << 1"      );
	print_shift(&tmp_short, short_size, "unsigned short int << 1");
	print_shift(&tmp_long,  long_size,  "unsigned long int << 1" );

	printf("According to ANSI C x >> 1 must fill the vacated bit with 0\n"
	       "for unsigned quantities.  Signed quantities may fill the\n"
	       "vacant bit with the sign bits or 0 depndig on machine.\n");
	tmp_char   = end_char  >> 1;
	tmp_int    = end_int   >> 1;
	tmp_short  = end_short >> 1;
	tmp_long   = end_long  >> 1;
	print_shift(&tmp_char,  char_size,  "unsigned char >> 1"     );
	print_shift(&tmp_int,   int_size,   "unsigned int >> 1"      );
	print_shift(&tmp_short, short_size, "unsigned short int >> 1");
	print_shift(&tmp_long,  long_size,  "unsigned long int >> 1" );

	printf("Signed values [minus int %i plus int %i]\n",
	       end_minus_int, end_plus_int);
	tmp_minus_int = end_minus_int >> 1;
	tmp_plus_int  = end_plus_int  >> 1;
	print_shift(&end_minus_int,  int_size,  "(minus) signed int"     );
	print_shift(&tmp_minus_int,  int_size,  "(minus) signed int >> 1");
	print_shift(&end_plus_int,   int_size,  "(plus)  signed int"     );
	print_shift(&tmp_plus_int,   int_size,  "(plus)  signed int >> 1");

	return 0;
}

/*
 *** Example output on an IBM PowerPC (big endian)
sizeof size_t             = [8]
sizeof unsigned char	  = [1]
sizeof unsigned int	  = [4]
sizeof unsigned short int = [2]
sizeof unsigned long int  = [8]
sizeof float              = [4]
sizeof double             = [8]
sizeof char *             = [8]
sizeof void *             = [8]
Data representation: [ 01 ] unsigned char [1] [1]
Data representation: [ 01 02 03 04 ] unsigned int [16909060] [1020304]
Data representation: [ 01 02 ] unsigned short int [258] [102]
Data representation: [ 01 02 03 04 05 06 07 08 ] unsigned long int
[72623859790382856] [102030405060708]
Data representation: [ 01 02 03 04 ] float [0.000000] [3820406080000000]
Data representation: [ 01 02 03 04 05 06 07 08 ] double [0.000000]
[102030405060708]
According to ANSI C x << 1 must be equal to x * 2,
therefore the vacated bit must be filled with 0.
Data representation: [ 02 ] unsigned char << 1
Data representation: [ 02 04 06 08 ] unsigned int << 1
Data representation: [ 02 04 ] unsigned short int << 1
Data representation: [ 02 04 06 08 0a 0c 0e 10 ] unsigned long int << 1
According to ANSI C x >> 1 must fill the vacated bit with 0
for unsigned quantities.  Signed quantities may fill the
vacant bit with the sign bits or 0 depndig on machine.
Data representation: [ 00 ] unsigned char >> 1
Data representation: [ 00 81 01 82 ] unsigned int >> 1
Data representation: [ 00 81 ] unsigned short int >> 1
Data representation: [ 00 81 01 82 02 83 03 84 ] unsigned long int >> 1
Signed values [minus int -5 plus int 5]
Data representation: [ ff ff ff fb ] (minus) signed int
Data representation: [ ff ff ff fd ] (minus) signed int >> 1
Data representation: [ 00 00 00 05 ] (plus)  signed int
Data representation: [ 00 00 00 02 ] (plus)  signed int >> 1

 *** Example output on Intel x86_64 (little endian)
sizeof size_t             = [8]
sizeof unsigned char      = [1]
sizeof unsigned int       = [4]
sizeof unsigned short int = [2]
sizeof unsigned long int  = [8]
sizeof float              = [4]
sizeof double             = [8]
sizeof char *             = [8]
sizeof void *             = [8]
Data representation: [ 01 ] unsigned char [1] [1]
Data representation: [ 01 02 03 04 ] unsigned int [67305985] [4030201]
Data representation: [ 01 02 ] unsigned short int [513] [201]
Data representation: [ 01 02 03 04 05 06 07 08 ] unsigned long int
[578437695752307201] [807060504030201]
Data representation: [ 01 02 03 04 ] float [0.000000] [400c58]
Data representation: [ 01 02 03 04 05 06 07 08 ] double [0.000000] [400c58]
According to ANSI C x << 1 must be equal to x * 2,
therefore the vacated bit must be filled with 0.
Data representation: [ 02 ] unsigned char << 1
Data representation: [ 02 04 06 08 ] unsigned int << 1
Data representation: [ 02 04 ] unsigned short int << 1
Data representation: [ 02 04 06 08 0a 0c 0e 10 ] unsigned long int << 1
According to ANSI C x >> 1 must fill the vacated bit with 0
for unsigned quantities.  Signed quantities may fill the
vacant bit with the sign bits or 0 depndig on machine.
Data representation: [ 00 ] unsigned char >> 1
Data representation: [ 00 81 01 02 ] unsigned int >> 1
Data representation: [ 00 01 ] unsigned short int >> 1
Data representation: [ 00 81 01 82 02 83 03 04 ] unsigned long int >> 1
Signed values [minus int -5 plus int 5]
Data representation: [ fb ff ff ff ] (minus) signed int
Data representation: [ fd ff ff ff ] (minus) signed int >> 1
Data representation: [ 05 00 00 00 ] (plus)  signed int
Data representation: [ 02 00 00 00 ] (plus)  signed int >> 1

*/

