/* temoios.h prompt */
#include <portable.h>

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <ctype.h>

#define CMDHISTORY 13
#define CMDLENGHT 600
private char getche(void);
private void insertIn(char *str, int lenght, int position, char letter);
private void deleteField(char *str, int field, int lenght);
private bool isFirstWord(char *str, int cursor, int *wordstart);
private char *prompt(void);

/** This simulates a non cannonical input.
 *  Meaning that your shell will not have any control over
 *  the way you input from the keyboard.
 */
private char getche(void)
{
    /* The termios struct defines how input and output wil be processed */
    struct termios newterm, oldterm;
    char c;

    /* First we get the actual atributes and store them in oldterm */
    tcgetattr(STDIN_FILENO, &oldterm);
    newterm = oldterm;

    /* Now we set our flags to a non cannonical input */
    newterm.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR |
        IGNCR | ICRNL | IXON);
    newterm.c_oflag &= ~OPOST;
    newterm.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN);
    newterm.c_cflag &= ~(CSIZE | PARENB);
    newterm.c_cflag |= CS8;

    /* On some operating sytems VIM is defined to accept more data
     * than one character (common on UNIX non Linux system).
     * Therefore we define it to always return after getting
     * one char of data. */
    newterm.c_cc[VMIN] = sizeof(char);
    newterm.c_cc[VTIME] = 0;

    /* cfmakeraw(&newterm); */ /* If needed to deactivate special keys */

    /* Setting the terminal attributes to ours, getting the char
     * and setting back */
    tcsetattr(STDIN_FILENO, TCSANOW, &newterm);

    c = (char)getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldterm);

    return c;
}

/* Change the command line, after moving to the middle of it*/
private void insertIn(char *str, int lenght, int position, char letter)
{
    while(lenght != position)
    {
        str[lenght + 1] = str[lenght];
        lenght--;
    }

    str[lenght + 1] = str[lenght];
    str[lenght] = letter;
}

/* Deletion of characters using DEL and backspace */
private void deleteField(char *str, int field, int lenght)
{
    while(field != lenght)
    {
        str[field] = str[field + 1];
        field++;
    }

    str[lenght] = EOS;
}

/* the first word may be automatically completed */
private bool isFirstWord(char *str, int cursor, int *wordstart)
{
    int count = 0;
    bool ret = FALSE;

    while(' ' == str[count])
        count++;

    if(count > cursor)
        ret = FALSE;
    else
        *wordstart = count;

    while(' ' != str[count] && count < cursor)
        count++;

    if(cursor == count)
        ret = TRUE;

    /* Debug internal to the function, important because
     * the fields searched may generate a segmentation fault
     * if too much is needed to be printed */

    /*printf(" wordstart = %d, cursor internal = %d, count = %d\n",
        *wordstart, cursor, count);
*/
    return ret;
}

/* keymaping to look like a real prompt */
private char *prompt(void)
{
    /* Memory for the last used commands */
    static char command[CMDHISTORY][CMDLENGHT];
    char printed[CMDLENGHT];
    char c;
    char *prompt = " ctxadmin > ";
    static int i;
    int j = 0, counter;
    int cursor = 0, cmdlenght = 0;

    const char options[][20] = {"b", "boot", "ldcf", "loadconf", "unldcf",
        "unloadconf", "s", "shutdown", "pcp", "pcprocesses", "bc",
        "bootclients", "sc", "shutdownclients", "sa", "stopatm", "ba",
        "bootatm", "sp", "stopprocess", "bp", "bootprocess", "page",
        "paginate", "q", "quit", "!", "h", "help", ""};

    bool more_lines = FALSE;
    bool is_key_comb = FALSE;
    bool wasESC = FALSE;
    bool possibleDel = FALSE;

    static bool firstTime = TRUE;

    int columns, promptlen;
    static int currentCmd = 0;
    int affloat = 0;

    /* Cleaning the buffers for the commands */
    if(TRUE == firstTime)
    {
        for(i = 0; i < CMDHISTORY; i++)
            memset(command[i], 0, sizeof(command[i]));

        i = 0;
        firstTime = FALSE;
    }

    /* If someday we want to change the name of the prompt we need it's
     * lenght, not to execute it everytime it's needed */
    promptlen = strlen(prompt);

    /* We are getting the size of the terminal to know when to slide the
     * line in our prompt */
    if(EOF == sscanf(getenv("COLUMNS"), "%d", &columns))
    {
        printf("Could not find variable COLUMNS, setting ");
        printf("to default (80)\n");
        columns = 79; /* Actually we don't want the last column */
    }
    else
        columns--; /* Again we don't want the last column */

    while(1)
    {
        /* Incredibly useful for testing the key combinations!
         * Please don't remove. */

        printf("\n Before hexa = %x, cmdlenght = %d, cursor = %d, i = %d \
affloat = %d\n", c, cmdlenght, cursor, i, affloat);

        /* Cleaning and creating the printing buffer */
        memset(printed, 0, sizeof(printed));
        strncpy(printed, command[i] + affloat, columns - promptlen);

        printf("\r");

        for(counter = 0; counter < columns; counter++)
            printf(" ");

        printf("\r%s%s", prompt, printed);

        printf("\r%s", prompt);
        for(j = 0; j < cursor - affloat; j++)
            printf("%c", printed[j]);

        c = getche();

        /* Incredibly useful for testing the key combinations!
         * Please don't remove. */

        printf("\n After hexa = %x, cmdlenght = %d, cursor = %d, i = %d \
affloat = %d\n", c, cmdlenght, cursor, i, affloat);
        for(counter = 0; counter < CMDHISTORY; counter++)
            printf("command %d = %s\n", counter, command[counter]);

        /* Ascii carriage return or new line / line feed to finish */
        if(0x0d == c || 0x0a == c)
        {
            /*printf("\nprinting command:\n");
            printf("%s\n", command[i]);*/

            if(i != currentCmd)
                strcpy(command[currentCmd], command[i]);

            i = (currentCmd + 1) % CMDHISTORY;
            currentCmd = i;
            cmdlenght = cursor = 0;
            memset(command[i], 0, sizeof(command[i]));
            affloat = 0;
            break;
        }

	/* Check for key ascii combinations (like arrow keys) */
	if(TRUE == wasESC)
	{
            /* Have we already started a key combination? */
	    if(TRUE == is_key_comb)
            {
                /* 'A' for arrow up */
                if(0x41 == c)
                {
                    i = (0 == i ? CMDHISTORY - 1 : i - 1);
                    if(i == currentCmd)
                        i = (i + 1) % CMDHISTORY;

                    cmdlenght = cursor = strlen(command[i]);
                    /* That magical 1 means that we are not using
                     * the last column but we need to put it in this
                     * expression to find from where we print */
                    affloat =
                        strlen(command[i]) > columns - promptlen ?
                        strlen(command[i]) + promptlen - columns + 1 : 0;
                    wasESC = FALSE;
                    is_key_comb = FALSE;
                    continue;
                }

                /* 'B' for arrow down*/
		else if(0x42 == c)
                {
                    if(i != currentCmd)
                    {
                        i = (i + 1) % CMDHISTORY;
                        cmdlenght = cursor = strlen(command[i]);
                    }

                    /* That magical 1 means that we are not using
                     * the last column but we need to put it in this
                     * expression to find from where we print */
                    affloat =
                        strlen(command[i]) > columns - promptlen ?
                        strlen(command[i]) + promptlen - columns + 1 : 0;
                    wasESC = FALSE;
                    is_key_comb = FALSE;
                    continue;
                }

                /* 'C' for arrow right */
		else if(0x43 == c)
                {
                    cursor = (cursor < cmdlenght ? cursor + 1 : cursor);

                    /* This magical 1 is for the sliding before
                     * reaching the end of the command */
                    if(cursor == affloat + columns - promptlen - 1
                        && cursor != cmdlenght)
                        affloat++;

                    wasESC = FALSE;
                    is_key_comb = FALSE;
                    continue;
                }

                /* 'D' for arrow left */
		else if(0x44 == c)
                {
                    cursor = (0 != cursor ? cursor - 1 : 0);

                    /* Here we need the magical 1 because we need to
                     * go back from the first printed char otherwise
                     * it appears a bit strange */
                    if(affloat - 1 == cursor && affloat)
                        affloat--;

                    wasESC = FALSE;
                    is_key_comb = FALSE;
                    continue;
                }

                /* '3' for first char of delete */
		else if(0x33 == c)
                {
                    possibleDel = TRUE;
                    continue;
                }

                /* '~' for second char of delete */
		else if(TRUE == possibleDel && 0x7e == c)
                {
                    if(cursor != cmdlenght)
                    {
                        deleteField(command[i], cursor, cmdlenght);
                        cmdlenght--;
                    }
                    wasESC = FALSE;
                    is_key_comb = FALSE;
                    possibleDel = FALSE;
                    continue;
                }
            }

            /* Key combinations start with <ESC> + '[' */
	    else if(0x5b == c)
            {
                is_key_comb = TRUE;
                continue;
            }
        }

        /* Ascii backspace or ascii del (Linux) for backspace key */
	if(0x08 == c || 0x7f == c)
        {
            if(cmdlenght == cursor)
            {
                cmdlenght = (0 != cmdlenght ? cmdlenght - 1 : 0);
                cursor = (0 != cursor ? cursor - 1 : 0);
                command[i][cmdlenght] = '\0';
            }
            else
                if(cursor != 0)
                {
                    deleteField(command[i], cursor - 1, cmdlenght);
                    cursor--;
                    cmdlenght--;
                }

            continue;
        }

        /* Ascii ESC (for key combinations */
	if(0x1b == c)
        {
	    wasESC = TRUE;
	    continue;
        }

        /* Ctrl+D combination for clearing current line */
        if(0x04 == c)
        {
            i = currentCmd;
            memset(command[i], 0, sizeof(command[i]));
            cmdlenght = cursor = 0;
            continue;
        }

        /* Horizontal tab for auto completion of commands */
        if(0x09 == c)
        {
            int wordstart;
            if(TRUE == isFirstWord(command[i], cursor, &wordstart))
            {
                int matches = 0, match;
                memset(printed, 0, sizeof(printed));

                for(counter = 0; options[counter][0] != '\0'; counter++)
                {
                    if(0 == strncmp(command[i] + wordstart, options[counter],
                        cursor - wordstart))
                    {
                        matches++;
                        match = counter;
                        strcat(printed, options[counter]);
                        strcat(printed, "\n");
                    }
                }

                /* If 1 match complete the command */
                if(1 == matches)
                    for(counter = cursor - wordstart;
                        options[match][counter] != '\0'; counter++)
                    {
                        insertIn(command[i], cmdlenght, cursor,
                            options[match][counter]);
                        cursor++;
                        cmdlenght++;
                    }

                /* If more matches print them */
                else if(1 < matches)
                    printf("\n%s", printed);
            }

            continue;
        }

        /* Finally this one prints any other (printable) characters */
	if(isprint(c))
        {
            if(cmdlenght == cursor)
            {
                command[i][cmdlenght] = c;
                cmdlenght++;
                cursor++;
            }
            else
            {
                insertIn(command[i], cmdlenght, cursor, c);
                cursor++;
                cmdlenght++;
            }

            /* If we are adding at the end of the line slide foward
             * if in the middle slide backwards */
            if(promptlen + cmdlenght >= columns) 
            {
                affloat++;

                /* The magical 2 is for not updating affloat if
                 * getting o close to columns (2 fields), then
                 * start adding the chars backwards */
                if(cursor != cmdlenght &&
                    cursor != affloat + columns - promptlen - 2)
                    affloat--;
            }
                    
        }
    }

    return command[0 == i ? CMDHISTORY - 1 : i - 1];
}

public int main(int argc, char *argv)
{
    char *cmd;

    while(1)
    {
        cmd = prompt();
        printf("Command received:\n");
        printf("%s\n", cmd);
    }

    return 0;
}

