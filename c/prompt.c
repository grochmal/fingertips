#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <ctype.h>

/* This simulates a non cannonical input.
 * Meaning that your shell will not have any control over
 * the way you input from the keyboard. */
char getche(void)
{
    /* The termios struct defines how input and output wil be processed */
    struct termios newterm, oldterm;
    char c;

    /* First we get the actual atributes and store them in oldterm */
    tcgetattr(STDIN_FILENO, &oldterm);
    newterm = oldterm;

    /* Now we set our flags to a non cannonical input */
    /*newterm.c_iflag &= ~(ICANON);*/ /* Actually it doesn't exist! */
    /*newterm.c_oflag &= ~OPOST;*/ /* Not needed anymore */
    newterm.c_cflag &= ~(ICANON);
    /*newterm.c_lflag &= ~(ICANON);*/
    newterm.c_lflag &= ~(ICANON | NOFLSH | ECHO);

    /* On some operating sytems VIM is defined to accept more data
     * than one character (common on UNIX non Linux system).
     * Therefore we define it to always return after getting
     * one char of data. */
    newterm.c_cc[VMIN] = sizeof(char);
    newterm.c_cc[VTIME] = 0;

    cfmakeraw(&newterm); /* If needed to deactivate special keys */

    /* Setting the terminal attributes to ours, getting the char
     * and setting back */
    tcsetattr(STDIN_FILENO, TCSANOW, &newterm);

    c = (char)getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldterm);

    return c;
}

/* Our non cannonical input need some keymapig to operate
 * as a normal prompt */
int main(int argc, char **argv)
{
    char command[600];
    char c;
    int i = 0;
    char is_key_comb = -1;
    char wasESC = -1;

    memset(command, 0, sizeof(command));

    printf(" ca > ");

    while(1)
    {
        c = getche();

        /* Incredibly useful for testing the key combinations! */
        printf("hexa = %x, \n", c);

        /* Ascii carriage return or new line / line feed to finish */
        if(0x0d == c || 0x0a == c)
            break;

	/* Check for key ascii combinations (like arrow keys) */
	if(1 == wasESC)
	{
            /* First check for <ESC> + <ESC> */
            /*if(0x1b == c)
            {
                printf("\nPrint Help!\n");
                wasESC = -1;
            }*/

            /* Have we already started a key combination? */
	    if(1 == is_key_comb)
            {
                /* 'A' for arrow up */
                if(0x41 == c)
                {
                    printf("\nPrevious Command\n");
                    wasESC = -1;
                    is_key_comb = -1;
                    continue;
                }

                /* 'B' for arrow down*/
		else if(0x42 == c)
                {
                    printf("\nNext Command\n");
                    wasESC = -1;
                    is_key_comb = -1;
                }

                /* 'C' for arrow right */
		else if(0x43 == c)
                {
                    printf("\nNext Char\n");
                    wasESC = -1;
                    is_key_comb = -1;
                }

                /* 'D' for arrow left */
		else if(0x44 == c)
                {
                    printf("\nPrevious Char\n");
                    wasESC = -1;
                    is_key_comb = -1;
                }

                /* '3' for arrow left */
		else if(0x44 == c)
                {
                    printf("\nPrevious Char\n");
                    wasESC = -1;
                    is_key_comb = -1;
                }
            }

            /* Key combinations start with <ESC> + '[' */
	    else if(0x5b == c)
            {
                is_key_comb = 1;
                continue;
            }
        }

        /* Ascii backspace or ascii del (Linux) for backspace key */
	if(0x08 == c || 0x7f == c)
        {
            i= (i != 0 ? i - 1 : 0);
            command[i] = '\0';
        }

	if( 0x1b == c )
        {
	    wasESC = 1;
            printf("\r");
            printf("                                                            ");
            printf("\r ca > %s", command);
	    continue;
        }

	if(isprint(c))
        {
            command[i] = c;
            i++;
            /*printf("debug!");*/
        }

        /*if ( 'D' == c && '[' == c_prv )
        {
            i= (i != 0 ? i - 2 : 0);
            command[i] = '\0';
            command[i+1] = '\0';
        }*/

        /* 600 spaces to clear the line */
        printf("\r");
        printf("                                                            ");
        printf("\r ca > %s", command);
    }

    printf("\nprinting command:\n");
    printf("%s\n", command);

    return 0;
}

