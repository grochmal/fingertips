/* print some stats about the data type sizes and endianess of the machine */

#include <stdio.h>

static void
print_byte_order (void *data, size_t data_size)
{
        char *p;
        size_t i;
        p = data;

        for (i = 1; i <= data_size; i++, p++)
                *p = (char)(i & 0xff);
        printf("Data representation: [ ");
        p = data;
        for (i = 1; i <= data_size; i++, p++)
                printf("%02x ", *p);
        printf("] ");
}

int
main (int argc, char **argv)
{
        size_t size_t_size = sizeof(size_t);
        size_t char_size   = sizeof(unsigned char);
        size_t int_size    = sizeof(unsigned int);
        size_t short_size  = sizeof(unsigned short int);
        size_t long_size   = sizeof(unsigned long int);
        size_t float_size  = sizeof(float);
        size_t double_size = sizeof(double);
        size_t cptr_size   = sizeof(char *);
        size_t vptr_size   = sizeof(void *);
        unsigned int       end_int    = ~0;
        unsigned short int end_short  = ~0;
        unsigned long int  end_long   = ~0;
        unsigned char      end_char   = ~0;
        float              end_float  = ~0;
        double             end_double = ~0;

        printf("sizeof size_t             = [%i]\n", size_t_size);
        printf("sizeof unsigned char      = [%i]\n", char_size);
        printf("sizeof unsigned int       = [%i]\n", short_size);
        printf("sizeof unsigned short int = [%i]\n", short_size);
        printf("sizeof unsigned long int  = [%i]\n", long_size);
        printf("sizeof float              = [%i]\n", float_size);
        printf("sizeof double             = [%i]\n", double_size);
        printf("sizeof char *             = [%i]\n", cptr_size);
        printf("sizeof void *             = [%i]\n", vptr_size);

        print_byte_order(&end_long, long_size);
        printf("value of unsigned long int [%li]\n", end_long);

        return 0;
}

