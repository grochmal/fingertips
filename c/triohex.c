#include <stdio.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

static char *_triohex = "0123456789abcdefghijklmnopqrstuvwxyz";
static int triohex2ulong(unsigned long *val, char *thex)
{
	int len = 0;
	unsigned long total = 0;
	unsigned long pval = 0;
	unsigned char *p;

unsigned long v = 0;
unsigned long x = x;
	len = strlen(thex);
	if (6 < len)
		return -1;

	len--;
	for (p = thex; *p; p++, len--) {
v = (unsigned long)strchr(_triohex, tolower(*p));
x = (unsigned long)_triohex;
		pval = (unsigned long)strchr(_triohex, tolower(*p))
		       - (unsigned long)_triohex;
printf("v [%lu], x [%lu], pval [%lu], len[%i]\n", v, x, pval, len);
		if (0 > pval)
			return -1;
		pval = pval * (unsigned long)pow(36, len);
		total += pval;
	}

	*val = total;
	return 0;
}

static char *strrev(char *to, char *from)
{
	int len = 0;
	char *p, *q;

	len = strlen(from);
	for (p = from + len - 1, q = to; p >= from; p--, q++)
		*q = *p;

	return to;
}

static int long2triohex(char *thex, int len, unsigned long val)
{
	int i = 0;
	int term = 6;
	unsigned long pval = 0;
	char reverse[6 + 1] = {0};
	char *p;

	if (6 > len)
		return -1;
	for (i = 1, p = reverse; term; i++, p++, term--) {
		pval = val % (unsigned long)pow(36, i)
		       / (unsigned long)pow(36, i - 1);
		*p = _triohex[pval];
	}

	(void)strrev(thex, reverse);
	thex[strlen(reverse)] = '\0';
	return 0;
}

#define MAXTRIOHEX  6
int main(void)
{
	int var = 0;
	int ret = 0;
	unsigned long l = 0;
	char triohex[MAXTRIOHEX + 1] = {0};
	ret = sscanf("Z051", "%i", &var);
	printf("var = [%i], ret = [%i]\n", var, ret);
	ret = triohex2ulong(&l, "3bCT91");
	printf("triohex2long [%lu] [%i]\n", l, ret);
	ret = long2triohex(triohex, MAXTRIOHEX, l);
	printf("long2triohex [%s] [%i]\n", triohex, ret);
	return 0;
}

