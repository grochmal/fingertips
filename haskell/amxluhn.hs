-- calculate card check digit - the amex way

getPairs :: [a] -> ([a], [a])
getPairs [] = ([], [])
getPairs (x:[]) = ([x], [])
getPairs (x:y:xs) = (x:retx, y:rety) where
    (retx, rety) = getPairs xs

sumDigits :: (Integral a) => a -> a
sumDigits x
    | x < 10 = x
    | otherwise = sumDigits $ (x `div` 10) + (x `mod` 10)

doubleSum :: (Integral a) => a -> a
doubleSum = sumDigits . (*2)

checkDigit :: (Integral a) => [a] -> a
checkDigit xs = let (firsts, seconds) = getPairs xs
                    sumFirsts = sum $ map doubleSum firsts
                    sumSeconds = sum seconds
                 in (sumFirsts + sumSeconds) `mod` 10

getRepeated :: (Num a) => a -> [a]
getRepeated x = (:) 0 $ replicate 8 x

allRepeated :: [[Integer]]
allRepeated = map getRepeated [0..9]

main = do
    let checkDigits = map checkDigit allRepeated
        fullNums = zipWith (++) allRepeated (map (\x -> [x]) checkDigits)
    mapM_ print fullNums

